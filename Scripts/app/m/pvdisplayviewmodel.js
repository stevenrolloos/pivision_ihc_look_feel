﻿// <copyright file="pvdisplayviewmodel.js" company="OSIsoft, LLC">
// Copyright © 2013-2017 OSIsoft, LLC. All rights reserved.
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF OSIsoft, LLC.
// USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT THE PRIOR EXPRESS WRITTEN
// PERMISSION OF OSIsoft, LLC.
//
// RESTRICTED RIGHTS LEGEND
// Use, duplication, or disclosure by the Government is subject to restrictions
// as set forth in subparagraph (c)(1)(ii) of the Rights in Technical Data and
// Computer Software clause at DFARS 252.227.7013
//
// OSIsoft, LLC.
// 1600 Alvarado Street, San Leandro, CA 94577
// </copyright>

/// <reference path="../../libs/knockout-3.2.0.js" />
/// <reference path="constants.js" />
/// <reference path="displayviewmodelhelper.js" />

window.Mobile = window.Mobile || {};
window.Mobile.ViewModels = window.Mobile.ViewModels || {};

window.Mobile.ViewModels.PVDisplayViewModel = (function (M) {
    'use strict';

    var displayId = ko.observable(0),
        base = new M.Helpers.DisplayViewModelHelper(),
        fromLinkTitle = false,
        displayNameInterval;

    var fireOpenedEvent = function (id, name) {
        if (id && name) {
            base.fireDisplayOpened.call(this, 'pv:\\\\' + id + '\\' + name);
        }
    };

    var fireOpenedEventForPB = function (id, name) {
        if (id && name) {
            base.fireDisplayOpened.call(this, 'pb:\\\\' + id + '\\' + name);
        }
    };

    base.displayTitle.subscribe(function (newValue) {
        if (!fromLinkTitle) {
            fireOpenedEvent(displayId(), newValue);
        }
    });

    displayId.subscribe(function (newValue) {
        if (!fromLinkTitle) {
            fireOpenedEvent(newValue, base.displayTitle());
        }      
    });


    // override the base behavoir for isBusy. PV Displays handle the busy indicator.
    base.isBusy = function () { return false; };

    var applyUrlParams = function (pageData) {
        var params = '';
        var paramDelimiter = '?';

        // ignore any properties we are appending to the outgoing URL
        var ignoreProps = ['id', 'mode', 'hidetoolbar'];

        for (var prop in pageData) {
            if (ignoreProps.indexOf(prop) === -1) {
                params += paramDelimiter + prop + '=' + pageData[prop];
                paramDelimiter = '&';
            }
        }
        
        params += paramDelimiter + 'redirect=false&mode=kiosk&hidetoolbar';
        return params;
    };

    var openDisplay = function (displayIdToOpen, displayName, pageData) {

        base.openDisplay();
        base.displayTitle(displayName);
        displayId(displayIdToOpen);

        var pivisualizationSite = base.baseUrl() + '#/Displays/' + displayIdToOpen + applyUrlParams(pageData);

        if (!!M.Constants.CROSSDOMAINSITE) {
            pivisualizationSite = M.Constants.CROSSDOMAINSITE + pivisualizationSite;
        }

        $('#PVDisplayFrame')
            .on('load', function () {
                if (displayName && !displayNameInterval) {
                    base.displayTitle(displayName);
                }
                var that = this;
                var previousURI;
                var readDisplayName = function () {
                    if (that.contentWindow.document.baseURI
                        && that.contentWindow.document.baseURI !== previousURI
                        && that.contentWindow.PIVisualization && that.contentWindow.PIVisualization.Utils
                        && that.contentWindow.document.title !== 'IHC') {
                        var uriParser = that.contentWindow.PIVisualization.Utils.parseDisplayURI(that.contentWindow.document.baseURI);
                        if (uriParser.isProcessBookDisplay) {
                            if (that.contentWindow.PBDisplays && that.contentWindow.PBDisplays.displayViewModel
                                && that.contentWindow.PBDisplays.displayViewModel.displayInfo
                                && that.contentWindow.PBDisplays.displayViewModel.displayInfo.name
                                && that.contentWindow.PBDisplays.displayViewModel.displayInfo.name()) {

                                displayName = that.contentWindow.PBDisplays.displayViewModel.displayInfo.name();
                            } else {
                               return;
                            }
                        } else {
                            displayName = that.contentWindow.document.title.replace('IHC - ', '');
                        }
                        updateDisplayTitleFromLink(uriParser.displayId, displayName, uriParser.isProcessBookDisplay);
                        previousURI = that.contentWindow.document.baseURI;
                    }
                };
                if (!M.Constants.CROSSDOMAINSITE && !displayNameInterval) {
                    displayNameInterval = setInterval(readDisplayName, 1000);
                }
            })
            .attr('src', pivisualizationSite);
    };

    var closeDisplay = function () {
        base.closeDisplay();
        displayId(0);
        clearInterval(displayNameInterval);
        displayNameInterval = null;
    };

    var updateDisplayTitleFromLink = function (dispId, dispTitle, isPbDisplay) {
        fromLinkTitle = true;
        displayId(dispId);
        base.displayTitle(dispTitle);
        if (isPbDisplay) {
            fireOpenedEventForPB(dispId, dispTitle);
        } else {
            fireOpenedEvent(dispId, dispTitle);
        }
        fromLinkTitle = false;
    };

    return base.subClass({
        /**
        * Retrieves and shows display's data items 
        * @param {String} display id 
        */
        openDisplay: openDisplay,

        /**
        * Stops the pump and clears the display data
        */
        closeDisplay: closeDisplay,

        /**
        * Returns the canonical display link for this display.
        */
        displayLink: function () {
            //var pbviewerWindow = document.getElementById('PBDisplayFrame');
            //var startTime = pbviewerWindow ? pbviewerWindow.contentWindow.PBDisplays.displayViewModel.TimeBar.getServerStartTime() : '';
            //var endTime = pbviewerWindow ? pbviewerWindow.contentWindow.PBDisplays.displayViewModel.TimeBar.getServerEndTime() : '';
            return encodeURIComponent(base.baseUrl() + '#/Displays/' + displayId() /*+ (displayId() ? '?StartTime=' + startTime + '&EndTime=' + endTime : '')*/);
        },

        /**
        * Returns url params for this display.
        */
        applyUrlParams: applyUrlParams
    });

})(window.Mobile);
