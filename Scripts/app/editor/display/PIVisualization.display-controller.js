﻿// <copyright file="PIVisualization.display-controller.js" company="OSIsoft, LLC">
// Copyright © 2014-2017 OSIsoft, LLC. All rights reserved.
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF OSIsoft, LLC.
// USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT THE PRIOR EXPRESS WRITTEN
// PERMISSION OF OSIsoft, LLC.
//
// RESTRICTED RIGHTS LEGEND
// Use, duplication, or disclosure by the Government is subject to restrictions
// as set forth in subparagraph (c)(1)(ii) of the Rights in Technical Data and
// Computer Software clause at DFARS 252.227.7013
//
// OSIsoft, LLC.
// 1600 Alvarado Street, San Leandro, CA 94577
// </copyright>

/// <reference path="../_references.js"/>

window.PIVisualization = window.PIVisualization || {};

(function (PV) {
    'use strict';

    angular.module(APPNAME)
        .controller('DisplayController', DisplayController);

    DisplayController.$inject = [
        '$document',
        '$location',
        '$q',
        '$rootScope',
        '$sanitize',
        '$scope',
        '$state',
        '$timeout',
        '$window',
        '$transitions',
        'appClipboard',
        'appData',
        'assetContext',
        'chronicleProvider',
        'CONSTANTS',
        'cursorSync',
        'dataPump',
        'dateTimeFormatter',
        'diffGenerator',
        'displayData',
        'displayProvider',
        'eventCache',
        'log',
        'paneSplitters',
        'PiToast',
        'routeParams',
        'timeProvider',
        'webServices',
        'symbolCatalog',
        'displayState',
        'eventSearchModel',
        'multiselectService',
        'toolTabsModel'
    ];

    function DisplayController(
        $document, $location, $q, $rootScope, $sanitize, $scope, $state, $timeout, $window, $transitions,
        appClipboard, appData, assetContext, chronicleProvider, CONSTANTS, cursorSync, dataPump, dateTimeFormatter,
        diffGenerator, displayData, displayProvider, eventCache, log, paneSplitters, PiToast, routeParams, timeProvider,
        webServices, symbolCatalog, displayState, eventSearchModel, multiselectService, toolTabsModel) {

        var DrawingStates = Object.freeze({
            NotDrawing: 0,
            Started: 1,
            Active: 2
        });

        var vm = this,
            chronicle = null,
            lastDiffedName = null,
            lastSavedState = null,
            lastSavedProperties = null,
            pasteCount = 1,
            displayLoaded = false,
            editorData = null,
            pendingDisplayName = '',
            cancelDrop = false,
            dropInStencil = false,
            deregisterTransitionHookFinish,
            deregisterTransitionHookStart,
            adHocSymbol,
            adHocPositions;

        // controller methods
        vm.alignSymbols = alignSymbols;
        vm.changeDrawingMode = changeDrawingMode;
        vm.changeShapeMode = changeShapeMode;
        vm.contextMenu = new PV.ContextMenu(vm);
        vm.copySymbol = copySymbol;
        vm.cutSymbol = cutSymbol;
        vm.deleteSymbol = deleteSymbol;
        vm.switchSymbolToType = switchSymbolToType;
        vm.switchSymbolToCollection = switchSymbolToCollection;
        vm.groupSelectedSymbols = groupSelectedSymbols;
        vm.distributeSymbols = distributeSymbols;
        vm.draw = draw;
        vm.endDrawing = endDrawing;
        vm.getAdHocDisplayArgs = getAdHocDisplayArgs;
        vm.getRuntimeSymbolData = displayProvider.getRuntimeSymbolData;
        vm.handleContextMessage = handleContextMessage;
        vm.handleMouseMove = handleMouseMove;
        vm.handleMouseUp = handleMouseUp;
        vm.handleTouchEnd = handleTouchEnd;
        vm.handleTouchMove = handleTouchMove;
        vm.isMultipleDataSource = isMultipleDataSource;
        vm.moveBackward = moveBackward;
        vm.moveForward = moveForward;
        vm.moveToBack = moveToBack;
        vm.moveToFront = moveToFront;
        vm.pasteSymbol = pasteSymbol;
        vm.propertyIntersection = propertyIntersection;
        vm.redo = redo;
        vm.saveAsDisplay = saveAsDisplay;
        vm.saveDisplay = saveDisplay;
        vm.setArrangeButton = setArrangeButton;
        vm.setKioskMode = setKioskMode;
        vm.setLayoutMode = setLayoutMode;
        vm.showConfigPane = showConfigPane;
        vm.startDrawing = startDrawing;
        vm.substituteDatasources = substituteDatasources;
        vm.swapAssets = swapAssets;
        vm.toggleArrangeMenu = toggleArrangeMenu;
        vm.toggleShapeMenu = toggleShapeMenu;
        vm.toggleToolbar = toggleToolbar;
        vm.touchStart = touchStart;
        vm.undo = undo;
        vm.ungroupSymbols = ungroupSymbols;
        vm.dropOn = dropOn;
        vm.dragOver = dragOver;
        vm.dragOut = dragOut;
        vm.addSymbolsFromDrop = addSymbolsFromDrop;
        vm.refreshSelectedSymbol = refreshSelectedSymbol;
        vm.editSelectedSymbol = editSelectedSymbol;
        vm.isCollectionInEditMode = isCollectionInEditMode;
        vm.isThisCollectionInEditMode = isThisCollectionInEditMode;
        vm.isSymbolExpandable = displayProvider.isSymbolExpandable;
        vm.expandSymbol = expandSymbol;
        vm.cleanup = cleanup;   //exposed for testing purposes
        vm.rubberbandStart = rubberbandStart;
        vm.showRubberbandSelect = multiselectService.isActive;
        vm.getSymbolByName = displayProvider.getSymbolByName;

        // activate start-up logic
        (function activate() {
            vm.canAlign = false;
            vm.canCut = false;
            vm.canDistribute = false;
            vm.canMove = false;
            vm.canMoveBack = false;
            vm.canMoveFront = false;
            vm.canPaste = false;
            vm.canRedo = false;
            vm.canUndo = false;
            vm.busyIndicator = false;
            vm.currentSymbol = {
                x: 0,
                y: 0,
                dataSources: ['']
            };
            vm.copySource = 'display';
            vm.pasteTarget = 'display';
            vm.drawingStartPoint = { x: 0, y: 0 };
            vm.drawingState = DrawingStates.NotDrawing;
            vm.isAdhocDisplay = false;            
            vm.shapeMode = 'rectangle';
            vm.showArrangeButton = false;
            vm.showArrangeMenu = false;
            vm.showShapeMenu = false;
            vm.showToolbar = false;
            vm.dropAccept = 'breadcrumblist, [data-item-drag-source="true"]';
            vm.rubberbandSelect = multiselectService.rubberband;
            vm.topOffset = 0;
            vm.prevArchivePos = 0;
            vm.timeoutPromiseForRequestUpdate = null;
            vm.defaultChangedSymbolsOnly = false,
            vm.requestDataUpdate = false;
            vm.contextMenuOptions = [{
                title: PV.ResourceStrings.FormatDisplayOption,
                mode: 'display'
            }, {
                title: PV.ResourceStrings.ConfigureAssetSwapOption,
                mode: 'displayAssetSwap'
            }];

            Object.defineProperty(vm, 'display', {
                get: function () {
                    return displayProvider.instance;
                }
            });

            Object.defineProperty(vm, 'displayThemeName', {
                get: function () {
                    return displayProvider.displayThemeName;
                }
            });

            Object.defineProperty(vm, 'selectedSymbol', {
                get: function () {
                    return displayProvider.getLastSelectedSymbol();
                }
            });

            Object.defineProperty(vm, 'selectedSymbols', {
                get: function () {
                    return displayProvider.getSelectedSymbols(true);
                }
            });

            Object.defineProperty(vm, 'hasMultipleSelectedSymbols', {
                get: function () {
                    return displayProvider.getSelectedSymbols().length > 1 && displayProvider.getSelectedSymbols(true, true).length > 1;
                }
            });

            Object.defineProperty(vm, 'symbolGroupParents', {
                get: function () {
                    return displayProvider.symbolGroupParents;
                }
            });

            Object.defineProperty(vm, 'drawingModeName', {
                get: function () {
                    return displayProvider.drawingModeName;
                }
            });

            Object.defineProperty(vm, 'isDrawingEnabled', {
                get: function () {
                    return displayProvider.drawingModeName !== 'select';
                }
            });

            Object.defineProperty(vm, 'showNewDisplayText', {
                get: function () {
                    return displayLoaded && !displayProvider.instance.Symbols.length && vm.drawingState === DrawingStates.NotDrawing;
                }
            });

            Object.defineProperty(vm, 'layoutMode', {
                get: function () {
                    return displayProvider.layoutMode;
                },
                set: function (val) {
                    displayProvider.layoutMode = !!val;
                }
            });

            Object.defineProperty(vm, 'kioskMode', {
                get: function () {
                    return displayProvider.kioskMode;
                },
                set: function (val) {
                    displayProvider.kioskMode = !!val;
                }
            });

            log.clearDisplayErrors();
            PiToast.hideAll();
            initializeAngularListeners();

            // Initialize body and documentElement objects to be used in ACT event handlers
            PV.SetDocumentElements();

            cursorSync.reset();

            $scope.$evalAsync(function () {
                initializeDisplay();
            });

            toolTabsModel.selectTab();

            deregisterTransitionHookStart = $transitions.onStart({}, function ($transition$) {
                var toState = $transition$.to();
                var toParams = $transition$.params("to");
                var fromState = $transition$.from();
                var fromParams = $transition$.params("from");

                var to = parseRoutingState(toState, toParams);
                var from = parseRoutingState(fromState, fromParams);

                if (!attemptToLeaveDisplay()) {
                    return false;
                }

                vm.cleanup(toState, toParams, fromState, fromParams);
            });

            deregisterTransitionHookFinish = $transitions.onFinish({}, function ($transition$) {
                if ($transition$.params("to").hasBackButton) {
                    var toStateName = $transition$.to().name;
                    var fromStateName = $transition$.from().name;
                    $timeout(function () {
                        $rootScope.$broadcast('backButtonSaveState', toStateName, fromStateName);
                    });
                }
            });

        })();

        function handleContextMessage(name, args) {
            if (vm[name]) {
                if (Array.isArray(args)) {
                    vm[name].apply(vm, args);
                } else {
                    vm[name](args);
                }
            } else {
                $rootScope.$broadcast(name, args);
            }
        }

        function touchStart(event) {
            event.stopPropagation();
            closeContextMenus();

            if (event.originalEvent.touches.length === 1) {
                startDrawing(event.originalEvent.touches[0]);

                // Allow display-level context menu to open after touch-hold
                if (vm.drawingState !== DrawingStates.NotDrawing) {
                    event.preventDefault();
                }
            }
        }

        function refreshSelectedSymbol(name) {
            dataPump.requestUpdateForSelectedSymbol(name);
        }

        function updateCanUndoRedoProperties() {
           vm.canUndo = chronicle.canUndo();
           vm.canRedo = chronicle.canRedo();
        }

        function editSelectedSymbol(symbol) {
            setLayoutMode(true);
            dataPump.stop();
            vm.savedPasteCount = pasteCount;
            pasteCount = 1;
            displayProvider.enterEditSelectedSymbol(symbol);
            vm.pasteTarget = 'collection';
            if (isCollectionInEditMode()) {
                refreshCanCut();
                refreshCanPaste();
            }

            chronicle.saveUndoPosition();
            updateCanUndoRedoProperties();
            dataPump.start();
        }

        function isCollectionInEditMode() {           
            return displayProvider.isCollectionInEditMode();
        }

        function isThisCollectionInEditMode(symbol) {
            return displayProvider.isThisCollectionInEditMode(symbol);
        }

        function exitEditSelectedSymbol() {
            if (isCollectionInEditMode()) {
                refreshSelectedSymbol(displayProvider.defaultCollectionSymbol.Name);
                refreshCanPaste();                
            }

            displayProvider.exitEditSelectedSymbol();
            vm.pasteTarget = 'display';
            pasteCount = vm.savedPasteCount;
            if (vm.savedClipboard) {
                appClipboard.setSymbols(vm.savedClipboard.symbols, vm.savedClipboard.attachments, vm.savedClipboard.requestId);
            }

            chronicle.resetUndoPosition();
            updateCanUndoRedoProperties();
        }

        function groupSelectedSymbols() {
            displayProvider.groupSelectedSymbols();
            refreshButtons();
        }

        function ungroupSymbols(name) {
            var symbol = displayProvider.getSymbolByName(name);
            displayProvider.selectGroupChildren(symbol);
            displayProvider.removeSymbol(name);
            refreshButtons();
        }

        function handleTouchMove(event) {
            handleMouseMove(event.originalEvent.touches[0]);
        }

        function handleTouchEnd(event) {
            handleMouseUp(event.originalEvent.changedTouches[0]);
        }

        function closeContextMenus() {
            var kendoContextMenu = $('#symbol-context-menu').data('kendoContextMenu');
            if (kendoContextMenu) {
                kendoContextMenu.close();
            }

            kendoContextMenu = $('#event-context-menu').data('kendoContextMenu');
            if (kendoContextMenu) {
                kendoContextMenu.close();
            }
        }

        function launchAdHocDisplay() {
            // set the selected symbols, start and end time and request id (if available)
            var symbolNames = displayProvider.getAdHocDisplaySymbolNames();
            $state.go('Shell.EditorDisplay', {
                openadhocdisplay: symbolNames,
                eventframepath: timeProvider.timeRangeEvent.path,
                starttime: timeProvider.getServerStartTime(),
                endtime: timeProvider.getServerEndTime(),
                requestid: displayProvider.requestId,
                asset: ''
            }, {
                inherit: true
            });
        }

        function clearSelectedSymbol() {
            displayProvider.selectSymbol(); // Clear any symbol selection
            refreshButtons();
            $scope.$emit('selectedSymbolChangeEvent');
        }

        function startDrawing(event) {
            //prevent drawing outside stencil editing area when in collection edit mode  
            if (vm.isCollectionInEditMode()) {
                if ($(event.target).parents('[data-collection-edit-symbolhost]').length === 0) {
                    return;
                }
            } 

            // enabling Crtl+drag rubberband select
            if (!event.ctrlKey) {
                clearSelectedSymbol();
            }

            if (((vm.layoutMode && vm.showToolbar) || displayProvider.getDrawingOption('alwaysAdd')) &&
                vm.drawingModeName !== 'select' &&
                vm.drawingState === DrawingStates.NotDrawing) {

                var scale = appData.zoomLevel;
                var displayPosition = displayProvider.getDisplayAreaPositionObject();
                vm.drawingStartPoint.x = (event.pageX - displayPosition.left) / scale;
                vm.drawingStartPoint.y = (event.pageY - displayPosition.top) / scale;
                vm.drawingStartPoint.Width = 0;
                vm.drawingStartPoint.Height = 0;

                vm.currentSymbol.dataSources = [];
                vm.currentSymbol.x = vm.drawingStartPoint.x;
                vm.currentSymbol.y = vm.drawingStartPoint.y;

                var preventDraw = !!displayProvider.getDrawingOption('preventDraw');

                vm.newShape = displayProvider.getTempShape(
                    vm.currentSymbol.x,
                    vm.currentSymbol.y,
                    preventDraw);

                vm.drawingState = DrawingStates.Started;
                $rootScope.$broadcast('shapeDrawingModeToggled', true);

                if (displayProvider.getDrawingOption('cancelRubberbandStart')) {
                    event.stopPropagation();
                }

                if (preventDraw) {
                    endDrawing(event);
                }
            }
        }

        function handleMouseMove(event) {
            draw(event);
        }

        function draw(event) {
            if (vm.drawingState !== DrawingStates.NotDrawing) {
                var scale = appData.zoomLevel;
                var displayPosition = displayProvider.getDisplayAreaPositionObject();
                var clientX = event.clientX - displayPosition.leftWithoutScroll;
                var clientY = event.clientY - displayPosition.topWithoutScroll;

                if (clientX < 0 || clientY < 0 || clientX > displayPosition.width || clientY > displayPosition.height) {
                    return;
                }

                vm.drawingState = DrawingStates.Active;

                var x = (event.clientX - displayPosition.left) / scale;
                var y = (event.clientY - displayPosition.top) / scale;
                var currentSymbol = vm.newShape;
                var currentRuntimeData = vm.getRuntimeSymbolData(vm.newShape.Name);
                var width = x - vm.drawingStartPoint.x;
                var height = y - vm.drawingStartPoint.y;
                var quad = 0;
                var theta = Math.atan(height / width);
                if (width > 0) {
                    if (height > 0) { // BR
                        quad = 1;
                    } else {  // TR
                        quad = 2;
                    }
                } else {
                    if (height > 0) { // BL
                        quad = 3;
                    } else { // TL
                        quad = 4;
                    }
                }

                if (event.shiftKey) {
                    var diagLine = true;
                    if ((theta <= Math.PI / 2 && theta >= Math.PI / 3 || theta < (Math.PI / 3) * -1 && theta > (Math.PI / 2) * -1 || theta < Math.PI / 6 && theta > (Math.PI / 6) * -1) && currentSymbol.SymbolType === 'line') {
                        diagLine = false;
                    }
                    if (!currentSymbol.Configuration.Points || diagLine) {
                        var aspectRatio = (currentSymbol.Configuration.originalHeight && currentSymbol.Configuration.originalWidth) ?
                                          (currentSymbol.Configuration.originalHeight / currentSymbol.Configuration.originalWidth) : 1;
                        if (quad === 2 || quad === 3) {
                            height = width * -1 * aspectRatio;
                        } else {
                            height = width * aspectRatio;
                        }
                    }

                    var sqrClientX = event.clientX - displayPosition.leftWithoutScroll;
                    var sqrClientY;

                    if (quad === 2 || quad === 4) {
                        sqrClientY = ((vm.drawingStartPoint.y * scale - (displayPosition.topWithoutScroll - displayPosition.top)) - Math.abs(width * scale));
                    } else {
                        sqrClientY = ((vm.drawingStartPoint.y * scale - (displayPosition.topWithoutScroll - displayPosition.top)) + Math.abs(width * scale));
                    }
                    if ((sqrClientX < 0 || sqrClientY < 0 || sqrClientX > displayPosition.width || sqrClientY > displayPosition.height) && diagLine) {
                        return;
                    }
                }

                if (width < 0 && !currentSymbol.Configuration.Points) {
                    if (Math.abs(width) < currentRuntimeData.def.minWidth) {
                        currentRuntimeData.position.left = vm.drawingStartPoint.x - currentRuntimeData.def.minWidth;
                        currentRuntimeData.position.width = currentRuntimeData.def.minWidth;
                    } else {
                        currentRuntimeData.position.left = vm.drawingStartPoint.x + width;
                        currentRuntimeData.position.width = Math.abs(width);
                    }
                } else {
                    currentRuntimeData.position.left = vm.drawingStartPoint.x;
                    if (width < currentRuntimeData.def.minWidth && !currentSymbol.Configuration.Points) {
                        currentRuntimeData.position.width = currentRuntimeData.def.minWidth;
                    } else {
                        currentRuntimeData.position.width = width;
                    }
                }

                if (height < 0 && !currentSymbol.Configuration.Points) {
                    if (Math.abs(height) < currentRuntimeData.def.minHeight) {
                        currentRuntimeData.position.top = vm.drawingStartPoint.y - currentRuntimeData.def.minHeight;
                        currentRuntimeData.position.height = currentRuntimeData.def.minHeight;
                    } else {
                        currentRuntimeData.position.top = vm.drawingStartPoint.y + height;
                        currentRuntimeData.position.height = Math.abs(height);
                    }
                } else {
                    currentRuntimeData.position.top = vm.drawingStartPoint.y;
                    if (height < currentRuntimeData.def.minHeight && !currentSymbol.Configuration.Points) {
                        currentRuntimeData.position.height = currentRuntimeData.def.minHeight;
                    }
                    else {
                        currentRuntimeData.position.height = height;
                    }
                }

                if (currentSymbol.SymbolType === 'arc') {
                    currentRuntimeData.def.updateArcPath(currentSymbol.Configuration, quad, currentRuntimeData.position.height, currentRuntimeData.position.width, Math.ceil(currentSymbol.Configuration.StrokeWidth / 2));
                }

                if (currentSymbol.Configuration.Points) {
                    var xf = x - vm.drawingStartPoint.x;
                    var yf = y - vm.drawingStartPoint.y;
                    if (event.shiftKey) {
                        if (theta <= Math.PI / 2 && theta >= Math.PI / 3) {
                            xf = 0;
                        } else if (theta < Math.PI / 3 && theta > Math.PI / 6) {
                            yf = xf;
                        } else if (theta < Math.PI / 6 && theta > (Math.PI / 6) * -1) {
                            yf = 0;

                        } else if (theta < (Math.PI / 6) * -1 && theta > (Math.PI / 3) * -1) {
                            yf = xf * -1;
                        } else if (theta < (Math.PI / 3) * -1 && theta > (Math.PI / 2) * -1) {
                            xf = 0;
                        }
                    }

                    var startPoint = {
                        X: 0, Y: 0
                    };
                    var endPoint = {
                        X: xf, Y: yf
                    };
                    if (endPoint.X < 0 || endPoint.Y < 0) {
                        currentSymbol.Configuration.Points = [endPoint, startPoint];
                    } else {
                        currentSymbol.Configuration.Points = [startPoint, endPoint];
                    }

                    currentRuntimeData.position.width = Math.abs(currentRuntimeData.position.width);
                    currentRuntimeData.position.height = Math.abs(currentRuntimeData.position.height);
                }
            }
        }

        function handleMouseUp(event) {
            endDrawing(event);
        }

        function endDrawing(event) {
            if (vm.drawingState === DrawingStates.NotDrawing) {
                return;
            }

            var currentSymbol = vm.newShape;
            var currentRuntimeData = vm.getRuntimeSymbolData(currentSymbol.Name);

            if (currentRuntimeData.position.width <= currentRuntimeData.def.minWidth && currentRuntimeData.position.height <= currentRuntimeData.def.minHeight &&
                currentRuntimeData.position.width >= 0 && currentRuntimeData.position.height >= 0 &&
                (vm.drawingState === DrawingStates.Started || currentSymbol.Configuration.Points)) {

                currentRuntimeData.position.width = currentSymbol.Configuration.originalWidth || 100;
                currentRuntimeData.position.height = currentSymbol.Configuration.originalHeight || 100;

                if (currentSymbol.Configuration.Points) {
                    currentRuntimeData.position.height = 1;
                    currentSymbol.Configuration.Points = [{ X: 0, Y: 0 }, {
                        X: 100, Y: 0
                    }];
                }
            } else if (currentSymbol.Configuration.Points) {
                var minX = 0;
                var minY = 0;
                var maxX = 0;
                var maxY = 0;

                currentSymbol.Configuration.Points.forEach(function (point) {
                    if (point.X < minX) {
                        minX = point.X;
                    }
                    if (point.X > maxX) {
                        maxX = point.X;
                    }
                    if (point.Y < minY) {
                        minY = point.Y;
                    }
                    if (point.Y > maxY) {
                        maxY = point.Y;
                    }
                });

                currentSymbol.Configuration.Points.forEach(function (point) {
                    point.X += Math.abs(minX);
                    point.Y += Math.abs(minY);
                });

                currentRuntimeData.position.top += minY;
                currentRuntimeData.position.left += minX;
                if (maxX - minX === 0) {
                    currentRuntimeData.position.width = 1;
                } else {
                    currentRuntimeData.position.width = maxX - minX;
                }

                if (maxY - minY === 0) {
                    currentRuntimeData.position.height = 1;
                } else {
                    currentRuntimeData.position.height = maxY - minY;
                }
            }

            var sym = displayProvider.addSymbol(currentSymbol.SymbolType, currentRuntimeData.position.left, currentRuntimeData.position.top, currentSymbol.Configuration.dataSources, currentSymbol.Configuration, true);
            displayProvider.selectSymbol(sym.Name);

            $scope.$emit('selectedSymbolChangeEvent', true);
            $rootScope.$broadcast('shapeDrawingModeToggled', false);

            vm.drawingState = DrawingStates.NotDrawing;
            vm.newShape = null;

            if (!vm.layoutMode) {
                setLayoutMode(true);
            }
        }

        function canSaveCheck() {
            return displayProvider.instance && displayProvider.instance.Symbols && displayProvider.instance.Symbols.length > 0;
        }

        function checkDirtyDisplay() {
            if (vm.kioskMode) {
                displayProvider.isDirtyDisplay = false;
                displayProvider.canSaveAs = false;
                displayProvider.canSave = false;
            } else {
                var hasSwappedAssets = Object.keys(assetContext.swappedAssets).length !== 0;
                var currentState = getCurrentState(hasSwappedAssets ? assetContext.swappedAssets : null);
                displayProvider.isDirtyDisplay = lastSavedState !== currentState;
                displayProvider.canSaveAs = canSaveCheck();

                var assetChange = false;
                if (!displayProvider.isDirtyDisplay && hasSwappedAssets) {
                    assetChange = lastSavedState !== getCurrentState();
                }

                displayProvider.canSave =
                    (displayProvider.isDirtyDisplay && displayProvider.canSaveAs)
                    || (!displayProvider.displayName && displayProvider.canSaveAs)
                    || !angular.equals(lastSavedProperties, getSavedProperties())
                    || assetChange;
            }
 
            updateCanUndoRedoProperties();

            // upon user change, if no data sources left, deactivate the time bar
            if (displayProvider.containsSymbolsWithDataSources) {
                $rootScope.$broadcast('dataSourcesExistToEnableTimebarEvent');
            } else {
                $rootScope.$broadcast('resetTimebarEvent', {
                    start: timeProvider.displayTime.start, end: timeProvider.displayTime.end
                });
            }
        }

        function changeDrawingMode(modeName, options) {
            if (vm.drawingModeName !== modeName && vm.drawingModeName === 'select') {
                clearSelectedSymbol();
            }

            displayProvider.drawingMode = angular.extend({ name: modeName }, options);
            vm.showShapeMenu = false;
        }

        function changeShapeMode(shapeName) {
            changeDrawingMode(shapeName);
            vm.shapeMode = shapeName;
        }

        function cleanup(toState, toParams, fromState, fromParams) {
            // listener cleanup
            angular.element($window).off('beforeunload');
            angular.element($window).off('resize', null, windowResizeHandler);
            
            setTitle(null);
            $rootScope.$broadcast('displayClosingEvent', toState, toParams, fromState, fromParams);

            // service cleanup
            dataPump.stop();
            chronicleProvider.reset();
            assetContext.init();
            timeProvider.init();
            displayProvider.init();

            timeProvider.onDisplayTimeChanged.unsubscribe(onDisplayTimeChanged);
            timeProvider.onDisplayTimeUpdated.unsubscribe(eventSearchModel.refreshChange);
        }

        function onDisplayTimeChanged(isEvent) {
            if (isEvent) {
                if (timeProvider.timeRangeEvent && timeProvider.timeRangeEvent.path) {
                    displayState.saveState('eventFramePath', timeProvider.timeRangeEvent.path);
                    displayState.removeState('displayTime');
                }
            } else {
                displayState.saveState('displayTime', timeProvider.displayTime);
                displayState.removeState('eventFramePath');

                // maybe the timeprovider itself should just clear the event path when a 
                // normal time range is set?     
                timeProvider.resetTimeRangeEventPath(false);
            }
        }

        function cutCopyDeleteSelectedSymbols(isCut, isDelete) {
            if (!vm.kioskMode) {
                var selectedSymbols = displayProvider.getSelectedSymbols(true, true, false, true);

                if (selectedSymbols.length < 1) {
                    return;
                }

                // Check if cut or delete is initiated in layout mode to determine symbol removal
                if ((isCut || isDelete) && vm.layoutMode) {
                    displayProvider.deleteSelectedSymbolSwaps(assetContext.swappedAssets);

                    // Delete all selected symbols from display
                    selectedSymbols.forEach(function (sym) {
                        displayProvider.removeSymbol(sym.Name);
                    });

                    // In case of 'cut symbol', clear selected symbol on display
                    displayProvider.selectSymbol();

                    // todo: Remove from Symbol Data

                    // todo: In future, we should have just 'diff' call which update
                    // server with latest display configuration and doesn't reply back with data
                    // we use 'difffordata' now
                    refreshButtons();
                    timeOutRequestUpdate();
                }

                // Check that delete was not initiated and cut was not initiated unless in layout mode
                if (!isDelete && (vm.layoutMode || !isCut)) {
                    var attachments = [];
                    var processedIds = [];
                    var buildAttachment = function (symbol) {
                        if (symbol && symbol.Configuration && symbol.Configuration.AttachmentId !== undefined && 
                            processedIds.indexOf(symbol.Configuration.AttachmentId) === -1) {
                            // todo: saving the attachment object as is into the clipboard works, but it may be a better idea to
                            // write this in the same format as we save to the server
                            var attachment = displayProvider.getAttachment(symbol.Configuration.AttachmentId, true);
                            if (attachment) {
                                attachments.push(attachment);
                                processedIds.push(symbol.Configuration.AttachmentId);
                            } else {
                                // this could happen if someone copies a symbol just as a display is opened and before the image
                                // has finished downloading (not that anyone would). In this case we just have the copied image 
                                // "forget" its image not the best solution, but waiting for all images to download and then adding 
                                // those to the clipboard may cause even weirder issues (like the clipboard population appearing to
                                // happen some time after the user invoked the action
                                delete symbol.Configuration.AttachmentId;
                            }
                        }
                    };

                    selectedSymbols.forEach(function (sym) {
                        buildAttachment(sym);
                        if (displayProvider.isCollection(sym.SymbolType) && sym.StencilSymbols && sym.StencilSymbols.length > 0) {
                            sym.StencilSymbols.forEach(function (stencilSymbol) {
                                buildAttachment(stencilSymbol);
                            });
                        }
                    });
                    
                    // reset the paste count on any cut or copy
                    pasteCount = 1;
                    if (vm.isCollectionInEditMode()) {
                        vm.copySource = 'collection';
                        vm.savedClipboard = null;
                    } else {
                        vm.copySource = 'display';
                    }

                    appClipboard.setSymbols(selectedSymbols, attachments, displayProvider.requestId);
                }
            }
        }

        function copySymbol() {
            cutCopyDeleteSelectedSymbols();
        }

        function cutSymbol() {
            cutCopyDeleteSelectedSymbols(true);
        }

        function deleteSymbol() {
            cutCopyDeleteSelectedSymbols(false, true);
        }

        function refreshCanMove() {
            vm.canMoveFront = displayProvider.canMoveFront();
            vm.canMoveBack = displayProvider.canMoveBack();
            vm.canMove = vm.layoutMode && (vm.canMoveFront || vm.canMoveBack);
        }

        function alignSymbols(alignment) {
            displayProvider.alignSelectedSymbols(alignment);
            refreshCanAlign();
            vm.showArrangeMenu = false;
        }

        function distributeSymbols(direction) {
            displayProvider.distributeSelectedSymbols(direction);
            refreshCanDistribute();
            vm.showArrangeMenu = false;
        }

        function moveForward() {
            displayProvider.moveSelectedSymbolsForward();
            refreshCanMove();
            vm.showArrangeMenu = false;
        }

        function moveBackward() {
            displayProvider.moveSelectedSymbolsBackward();
            refreshCanMove();
            vm.showArrangeMenu = false;
        }

        function moveToFront() {
            displayProvider.moveSelectedSymbolsToFront();
            refreshCanMove();
            vm.showArrangeMenu = false;
        }

        function moveToBack() {
            displayProvider.moveSelectedSymbolsToBack();
            refreshCanMove();
            vm.showArrangeMenu = false;
        }

        function getCurrentState(swappedAssets) {
            // save full server display object to track name changes, etc. (should NOT contain event search criteria)
            var serverDisp = angular.copy(displayProvider.generateServerDisplay(null, null, true, swappedAssets, null));
            // Remove search criteria details so that display is not marked as dirty
           
            if (serverDisp.DisplayProperties) {
                if (serverDisp.DisplayProperties.hasOwnProperty('EventFrameSearch')) {
                    delete serverDisp.DisplayProperties.EventFrameSearch;
                }
                if (serverDisp.DisplayProperties.hasOwnProperty('FitAll')) {
                    delete serverDisp.DisplayProperties.FitAll;
                }
                if (Object.keys(serverDisp.DisplayProperties).length === 0) {
                    delete serverDisp.DisplayProperties;
                }
            }

            // Remove symbol configuration details that should not cause dirty display
            var currentState = JSON.stringify(serverDisp, function (key, value) {
                if (key === 'Symbols') {
                    var symbolString = '';
                    serverDisp.Symbols.forEach(function (sym) {
                        var def = displayProvider.getRuntimeSymbolData(sym.Name).def;
                        if (def && def.isolateDirtyDisplayProperties) {
                            sym = angular.copy(sym);
                            def.isolateDirtyDisplayProperties(sym.Configuration);
                        }
                        symbolString += JSON.stringify(sym);
                    });
                    return symbolString;
                }
                return value;
            });

            return currentState;
        }

        function initializeAngularListeners() {

            timeProvider.onDisplayTimeChanged.subscribe(onDisplayTimeChanged);
            timeProvider.onDisplayTimeUpdated.subscribe(eventSearchModel.refreshChange);

            angular.element($window).on('beforeunload', function () {
                if (displayProvider.isDirtyDisplay && !vm.isAdhocDisplay && PV.isPublisher) {
                    return PV.ResourceStrings.DirtyDisplayUnload;
                }
            });

            angular.element($window).on('resize', windowResizeHandler);

            $scope.$on('$destroy', function () {
                deregisterTransitionHookStart();
                deregisterTransitionHookFinish();
            });

            // miscellaneous
            $scope.$on(appClipboard.ClipboardChanged, refreshCanPaste);

            $scope.$on('changeDrawingMode', function (event, modeName, options) {
                changeDrawingMode(modeName, options);
            });

            $scope.$on('closeContextMenusEvent', function () {
                closeContextMenus();
            });

            $scope.$on('closeShapeMenuEvent', function () {
                vm.showShapeMenu = false;
            });

            $scope.$on('onDropOverSymbolEvent', function (event, symbol, $draggedElement) {
                onDropOverSymbol(symbol, $draggedElement);
            });

            $scope.$on('setLayoutModeEvent', function (event, isLayoutMode) {
                setLayoutMode(isLayoutMode);
            });

            $scope.$on('launchAdHocDisplay', function () {
                launchAdHocDisplay();
            });


            // selection
            $scope.$on('selectAll', function () {
                displayProvider.selectSymbol();
                $scope.$apply(function () {
                    displayProvider.selectAll();
                    refreshButtons();
                });
            });

            $scope.$on('resolveItemsWithDatasourcesEvent', function (event, items, callback) {
                displayProvider.resolveItemsWithDatasources(items, callback);
            });

            $scope.$on('selectedSymbolChangeEvent', function (event, resetDrawingTool, skipApply) {
                // this is rather hackish, but it avoids an error that
                // can occur because sometimes this is called within a 
                // scope.apply, and sometimes not

                if (!$scope.$root)
                    console.log('warning: display appears to been loaded twice which is usually a sign of a problem');

                if (!$scope.$root || $scope.$root.$$phase === '$apply' || $scope.$root.$$phase === '$digest' || skipApply) {
                    refreshButtons();
                } else {
                    $scope.$apply(refreshButtons());
                }

                if (resetDrawingTool) {
                    changeDrawingMode('select');
                }
            });

            $scope.$on('cancelDropEvent', function () {
                cancelDrop = true;
            });

            $scope.$on('dropInStencil', function () {
                dropInStencil = true;
            });

            // escape key
            $scope.$on('escape', function (event, data, factor) {
                if (vm.isCollectionInEditMode()) {
                    vm.setLayoutMode(false);
                }
            });

            // arrows
            $scope.$on('downArrow', function (event, data, factor) {
                if (vm.layoutMode) {
                    $scope.$apply(moveSelectedSymbolsVertical(1 * factor));
                }
            });

            $scope.$on('upArrow', function (event, data, factor) {
                if (vm.layoutMode) {
                    $scope.$apply(moveSelectedSymbolsVertical(-1 * factor));
                }
            });

            $scope.$on('leftArrow', function (event, data, factor) {
                if (vm.layoutMode) {
                    $scope.$apply(moveSelectedSymbolsHorizontal(-1 * factor));
                }
            });

            $scope.$on('rightArrow', function (event, data, factor) {
                if (vm.layoutMode) {
                    $scope.$apply(moveSelectedSymbolsHorizontal(1 * factor));
                }
            });

            // cut/copy/paste/delete
            $scope.$on('copy', function () {
                $scope.$apply(copySymbol());
            });

            $scope.$on('cut', function () {
                $scope.$apply(cutSymbol());
            });

            $scope.$on('delete', function () {
                $scope.$apply(deleteSymbol());
            });

            $scope.$on('paste', function () {
                $scope.$apply(pasteSymbol());
            });

            // undo/redo
            $scope.$on('beginMultipleUndoEvents', function () {
                chronicleProvider.beginCompressedEvents();
            });

            $scope.$on('endMultipleUndoEvents', function () {
                chronicleProvider.endCompressedEvents();
            });

            $scope.$on('redo', function () {
                $scope.$apply(redo());
            });

            $scope.$on('undo', function () {
                $scope.$apply(undo());
            });

            $scope.$on('expandSymbol', function (messageType, data) {
                expandSymbol(data);
            });

            $scope.$on('openHyperlink', function (messageType, command) {
                if (command && command.URL) {
                    var isSafeDomain = checkDomain(command.URL);
                    var url = buildNavigationLink(command.URL, {}, isSafeDomain);
                    launchLink(url, command.newTab, isSafeDomain);
                }
            });

            // data refresh
            $scope.$on('refreshDataForAllSymbols', function () {
                timeOutRequestUpdate();
            });

            $scope.$on('refreshDataForChangedSymbols', function (event, forceUpdate) {
                timeOutRequestUpdate(true, forceUpdate);
            });

            $scope.$on('refreshDataforChangedSymbolsWithCursor', function () {
                dataPump.ext.requestDisplayData(cursorSync.date ? cursorSync.date.toISOString() : null);
            });

            $scope.$on('updateTrendCursors', function (event, cursorDate) {
                if (cursorDate) {
                    var symbols = displayProvider.symbolsAllowCursors();

                    if (symbols.length > 0) {
                        if (!cursorSync.wasUpdating) {
                            cursorSync.wasUpdating = timeProvider.getServerIsUpdating();
                        }
                        cursorSync.date = cursorDate;
                        dataPump.ext.requestCursorData(cursorDate.toISOString(), symbols);
                    }
                }
            });

            $scope.$on('updateZoomedTrend', function (event, callBack) {
                dataPump.ext.requestDisplayData(cursorSync.date ? cursorSync.date.toISOString() : null).then(
                    function () {
                        callBack();
                    },
                    function (error) {
                        callBack(error);
                    }
            );
            });

            // save
            $scope.$on('saveAsDisplayEvent', function (event, data) {
                saveAsDisplay(checkDisplayName(data.inputText), data.folderId);
            });

            $scope.$on('saveDisplayEvent', function (event, data) {
                // update the display name and invoke the onNameChange callback
                // this does not trigger a change in the watch object
                pendingDisplayName = checkDisplayName(data.inputText);
                var reload = onNameChange(); // returns true if name has changed, refresh required if name change occurred
                saveDisplay(reload);
            });

            $scope.$on('saveDialogOpenedEvent', function (event) {
                vm.busyIndicator = false;
            });

            $scope.$on('openSaveDialogEvent', function (event) {
                vm.busyIndicator = true;
            });

            $scope.$on('updateDisplayContext', function (event, params) {
                changeDisplayContext(params);
            });

            function checkDisplayName(displayName) {
                if (displayName && displayName.length > PV.ClientSettings.MaxDisplayNameLength) {
                    displayName = displayName.substr(0, PV.ClientSettings.MaxDisplayNameLength);
                }
                return displayName;
            }
        }

        function changeDisplayContext(params) {
            if (vm.substituteDatasources(params.asset, params.rootpath, assetContext.swappedAssets)) {
                dataPump.metadataManager.include = true;
                dataPump.requestUpdate();
            } else if (params.asset) {
                // Fall back to use related asset swap logic if simple asset substitution failed
                matchAssets(params.asset);
            }
        }

        function propertyIntersection(symbols) {
            // Find intersection of multiConfig flags on symbol definitions
            var multiConfigFlags = Object.keys(displayProvider.getRuntimeSymbolData(symbols[0].Name).def.formatMap || {});
            var i = 1;
            while (multiConfigFlags.length > 0 && i < symbols.length) {
                var rt = displayProvider.getRuntimeSymbolData(symbols[i].Name);
                if (rt) {
                    var newFlags = Object.keys(rt.def.formatMap || {});
                    multiConfigFlags = multiConfigFlags.filter(function (flag) {
                        return newFlags.indexOf(flag) !== -1;
                    });
                }

                i++;
            }

            return multiConfigFlags;
        }

        function expandSymbol(command) {
            if (vm.layoutMode || displayProvider.isEmbeddedAsPopup || !command || !command.symbolName) {
                return;
            }

            var sym = displayProvider.getSymbolByName(command.symbolName, true);
            if (!sym || !(sym.Configuration.LinkURL || displayProvider.isSymbolExpandable(sym.Name))) {
                return;
            }

            if (!command.popupTrend && sym.Configuration.LinkURL) {
                var params = {};
                if (sym.Configuration.IncludeTimeRange) {
                    params.starttime = timeProvider.displayTime.start.replace('.000Z', 'Z');
                    params.endtime = timeProvider.displayTime.end.replace('.000Z', 'Z');
                }

                if (sym.Configuration.IncludeAsset) {
                    var datasources;
                    var assets;
                    
                    if (command.datasources) {
                        // Table rows a
                        datasources = command.datasources;
                                                   
                    } else if (sym.CollectionAsset) {
                        // Collection member
                        assets = [sym.CollectionAsset.substring(3)];

                    } else if (sym.Configuration.LinkAsset) {
                        // Static symbol with user-defined asset
                        assets = [sym.Configuration.LinkAsset.substring(3)];

                    } else {
                        datasources = (sym.DataSources || []).concat(sym.MSDataSources || []);
                    }

                    if (datasources && datasources.length > 0) {
                        assets = datasources.filter(function (asset) {
                            return asset.toLowerCase().indexOf('af:') === 0; //af attribute
                        }).map(function (asset) {
                            var index = asset.indexOf('|');
                            return asset.substring(3, index >= 0 ? index: undefined);
                        }).filter(function (asset, index, array) {
                            return array.lastIndexOf(asset) === index;
                        });
                    }
                            
                    if (datasources && datasources.length === 0 && (!assets || assets.length === 0)) {
                        // Static symbol or text link with no multistates - use all assets
                        assets = displayProvider.getDistinctAssetsFromSymbolDatasources(false, true);
                    }

                    if (assets && assets.length > 0) {
                        if (sym.Configuration.LinkURL.indexOf('/PBDisplays/') !== -1) {
                            params.currentelement = assets[0];
                        } else if (sym.Configuration.IncludeAsset === 'root') {
                            params.rootpath = assets[0];
                        } else {
                            params.asset = assets.join(';');
                        }
                    }
                }
                
                if (sym.Configuration.LinkURL === '#') {
                    changeDisplayContext(params);
                } else {
                    var url = sym.Configuration.LinkURL.trim();
                    var isSafeDomain = checkDomain(url);
                    url = buildNavigationLink(url, params, isSafeDomain);
                    launchLink(url, sym.Configuration.NewTab, isSafeDomain);
                }

            } else {
                var symbolList = displayProvider.isGroup(sym.SymbolType)
                        ? displayProvider.getRuntimeSymbolData(sym.Name).groupChildrenWithDataSources
                        : sym.Name;

                $rootScope.$broadcast('openPopupTrend', {
                    datasources: command.datasources || null,
                    id: routeParams.getParamValue('id') > -1 ? routeParams.getParamValue('id') : -1,  // could be 'New'
                    openpopup: symbolList,
                    prequestid: displayProvider.requestId,
                    pstarttime: timeProvider.getServerStartTime(),
                    pendtime: timeProvider.getServerEndTime(),
                    displayBackgroundColor : displayProvider.BackgroundColor
                });
            }
        }
        
        function buildNavigationLink(url, configParams, isRelativePath) {
            if (isRelativePath) {
                // Encode URL and existing parameters
                var existingParams = [];
                var query = url.indexOf('?');
                if (query > -1) {
                    existingParams = url.slice(query + 1).split('&');
                    existingParams.forEach(function (param, i) {
                        var equals = param.indexOf('=');
                        existingParams[i] = (equals > -1 ? tryToEncodeUri(param.slice(0, equals), true) + '=' : '') + tryToEncodeUri(param.slice(equals + 1), true);
                    });

                    url = tryToEncodeUri(url.slice(0, query));
                } else {
                    url = tryToEncodeUri(url);
                }

                // Include application switches
                getUrlSwitches(existingParams);

                url = buildUrl(url, existingParams);
            }

            // Include URL options set in config pane
            var delim = url.indexOf('?') === -1 ? '?': '&';
            for (var key in configParams) {
                url += delim + key + '=' + customEncodeUri(configParams[key]);
                delim = '&';
            }

            return url;
        }

        function getUrlSwitches(params) {
            params = params || [];
            var configured = '&' + params.join('&').toLowerCase();
            if (routeParams.isKiosk && configured.indexOf('&mode=') === -1) {
                params.push('mode=kiosk');
            }
            if (routeParams.hasTruthyParamValue('hidetoolbar') && configured.indexOf('&hidetoolbar') === -1) {
                params.push('hidetoolbar');
            }
            if (routeParams.hasTruthyParamValue('hidetimebar') && configured.indexOf('&hidetimebar') === -1) {
                params.push('hidetimebar');
            }
            if (routeParams.hasTruthyParamValue('redirect') && configured.indexOf('&redirect=') === -1) {
                params.push('redirect=' + routeParams.getParamValue('redirect'));
            }
        }

        function buildUrl(url, params) {
            if (params.length > 0) {
                url += ((url.indexOf('?') === -1) ? '?' : '&') + params.join('&');
            }
            return url;
        }

        function tryToEncodeUri(str, isParam) {
            try {
                if (isParam) {
                    // Users should encode '+' as '%2B' themselves, otherwise '+' is interpreted as space
                    str = str.replace(/\+/g, ' ');

                    // Parameters: decode followed by encode to add custom encoding without double encoding
                    return customEncodeUri(decodeURIComponent(str));
                } else {
                    // Path: encode if there are no preexisting encodings (this assumes percent encodings are valid; invalid encodings cause an error in angular)
                    return str.indexOf('%') === -1 ? encodeURI(str) : str;
                }
            } catch (e) {
                // In case of malformed URI, leave url unchanged
                return str;
            }
        }

        // Encoding adapted from Angular. If our encoding does not match Angular's, it generates an extra history state.
        function customEncodeUri(val) {
            return encodeURIComponent(val).
                       replace(/%40/gi, '@').
                       replace(/%3A/gi, ':').
                       replace(/%24/g, '$').
                       replace(/%2C/gi, ',').
                       replace(/%3B/gi, ';');
        }

        function launchLink(url, newTab, isSafeDomain) {
            try {
                // launch link from <a> element with rel="noreferrer" in order to prevent tabnabbing/phishing bug
                var div = document.querySelector('.linkURLLauncher');
                var anchor = document.createElement('a');
                anchor.setAttribute('href', url);
                anchor.target = newTab ? '_blank' : '_self';
                anchor.rel = 'noreferrer';

                if (PV.navigationLinkSecurityOverride) {
                    // no sanitization
                    div.innerHTML = anchor.outerHTML;
                    div.firstChild.click();
                    div.innerHTML = "";
                    return;
                }

                div.innerHTML = $sanitize(anchor.outerHTML);
                
                // Show warning message if needed (unless href is cleared by $sanitize)
                if ($(div.innerHTML)[0].href && (isSafeDomain || $window.confirm(PV.stringFormat(PV.ResourceStrings.DomainChangeWarning, '\n' + $(div.innerHTML)[0].href + '\n')))) {
                    div.firstChild.click();
                }
                div.innerHTML = "";
            } catch (err) {}
        }

        function checkDomain(url) {
            // This regular expression test returns true if url is an absolute path, false if url is a relative path
            var isSafeDomain = !(new RegExp('^(?:[a-z]+:)?//', 'i')).test(url);
            if (!isSafeDomain) {
                // remove port number
                var sourceSegments = $window.location.host.split(':')[0].split('.');

                // remove 'www'
                /^www([0-9])?$/.test(sourceSegments[0]) && sourceSegments.splice(0, 1);

                // URI Generic syntax expression
                // https://tools.ietf.org/html/std66#appendix-B
                var regex = /^(([^:\/?#]+):)?(\/\/([^\/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?/i;

                // 4th item is the authority part of the url
                // If authentication section is present, ignore it.
                var targetHost = regex.exec(url)[4].split('@')[1] || regex.exec(url)[4].split('@')[0];

                // Check if the current hostname is an IP address
                if (sourceSegments.length === 4 && sourceSegments.every(function (part) { return part !== '' && !isNaN(+part) && (+part >= 0) && (part < 256); })) {
                    isSafeDomain = $window.location.host === targetHost;
                }
                else {
                    var targetSegments = targetHost.split(':')[0].split('.');

                    // remove 'www'
                    /^www([0-9])?$/.test(targetSegments[0]) && targetSegments.splice(0, 1);

                    // Compare last two parts of hostname if target or source hostname has only two parts. Compare last three parts otherwise
                    var minLength = Math.min(sourceSegments.length, targetSegments.length);
                    var offset = minLength > 2 ? 3 : minLength;
                    targetSegments.splice(0, targetSegments.length - offset);
                    sourceSegments.splice(0, sourceSegments.length - offset);
                    isSafeDomain = (sourceSegments.length == targetSegments.length) &&
                    // Compare port numbers
                    ($window.location.host.split(':')[1] === targetHost.split(':')[1]) &&
                    sourceSegments.every(function (part, index) { return part === targetSegments[index]; });
                }
            }

            return isSafeDomain;
        }

        // checks states to see if it's OK to leave the display (prompts the use for unsaved changes)
        // true means it is OK to leave the display view, false means stay on this display
        function attemptToLeaveDisplay() {
            if (displayProvider.isDirtyDisplay && !vm.isAdhocDisplay && PV.isPublisher) {
                return $window.confirm(PV.ResourceStrings.DirtyDisplayNavigate);
            } 
            return true;
        }

        function parseRoutingState(state, params) {
            var isDisplayState = state.name.lastIndexOf('Shell', 0) === 0;
            var isAdHoc = !!params.openadhocdisplay;
            var isEventComparison = state.name === 'Shell.EventComparison';
            var isEditorDisplay = state.name === 'Shell.EditorDisplay';
            return {
                display: isDisplayState,
                id: isDisplayState ? params.id : null,
                adHoc: isAdHoc,
                eventComparison: isEventComparison,
                editorDisplay: isEditorDisplay
            };
        }

        function limitKeyboardMoves(value) {
            if (value > 0) {
                return Math.min(value, 50);
            }
            return Math.max(value, -50);
        }

        function moveSelectedSymbolsVertical(direction) {
            direction = limitKeyboardMoves(direction);
            displayProvider.moveSelectedSymbols(0, direction);
            displayProvider.moveSymbolsCompleted();
        }

        function moveSelectedSymbolsHorizontal(direction) {
            direction = limitKeyboardMoves(direction);
            displayProvider.moveSelectedSymbols(direction, 0);
            displayProvider.moveSymbolsCompleted();
        }

        function showConfigPane(option) {
            if (option && option.mode) {
                vm.showShapeMenu = false;
                $rootScope.$broadcast('showConfigPane', option);
            }
        }

        function forceSplitterResize() {
            var splitter = paneSplitters.getShellSplitter();
            if (splitter) {
                $timeout(function () {
                    splitter.trigger('resize');
                    $(window).resize();
                }, 1);
            }
        }

        function setFooterHeight() {
            var footer = paneSplitters.getFooterSplitter();
            if (footer) {
                var timebar = $('#timebar-whole');
                if (timebar.length > 0) {
                    footer.options.panes[1].size = timebar[0].clientHeight;
                }
                footer.resize(true);
            }
        }

        function closeMenusOnWindowResize() {
            closeContextMenus();
            $scope.$emit('closeShapeMenuEvent');
        }

        function windowResizeHandler() {
            setFooterHeight();
            closeMenusOnWindowResize();
        }

        // Called when display is loaded or URL parameters change
        function startDataPumpForUrlParametersOrRange(start, end, eventFramePath) {

            var timeParameters = {
                starttime: routeParams.getParamValue('starttime') || start,
                endtime: routeParams.getParamValue('endtime') || end,
                tz: routeParams.getParamValue('tz') || undefined,
                eventframepath: routeParams.getParamValue('eventframepath') || eventFramePath,
                currentelement: routeParams.getParamValue('currentelement') || undefined
            };

            // If endtime is a blank string, but starttime is provided in the URL, it is probably a generated URL from AF.
            // We are requested to default the endtime in this situation.
            if (routeParams.hasTruthyParamValue('starttime') && routeParams.getParamValue('endtime') === '') {
                timeParameters.endtime = end || (timeProvider && timeProvider.displayTime && timeProvider.displayTime.end) || '';
            } else if (!routeParams.hasTruthyParamValue('starttime') || !routeParams.hasTruthyParamValue('endtime')) {
                timeParameters.starttime = start || (timeProvider && timeProvider.displayTime && timeProvider.displayTime.start) || '';
                timeParameters.endtime = end || (timeProvider && timeProvider.displayTime && timeProvider.displayTime.end) || '';
            }

            var isNewRange = false;
            if (timeParameters.eventframepath) {
                timeParameters.starttime = null;
                timeParameters.endtime = null;
                timeProvider.applyNewTimeRangeEventPath(timeParameters.eventframepath, false);
                isNewRange = true;
            } else {
                isNewRange = !!(timeParameters.starttime && timeParameters.endtime);
                if (timeParameters.starttime && timeParameters.endtime) {
                    timeProvider.resetTimeRangeEventPath();
                }
            }

            var currentElementExist = false;
            if (timeParameters.currentelement) {
                displayProvider.currentElement = timeParameters.currentelement;
                currentElementExist = true;
            }

            // Initiate data request if something important changed
            if (dateTimeFormatter.normalizeTimeZone(timeParameters.tz) !== dateTimeFormatter.tzClientOverride || isNewRange || currentElementExist) {
                dateTimeFormatter.tzClientOverride = timeParameters.tz;

                if (isNewRange && !timeProvider.timeRangeEvent.path) {
                    timeProvider.requestNewTime(timeParameters.starttime, timeParameters.endtime, false, true);
                }

                //in the case of an asset URL parameter, the update request will be made after the asset context switch
                if (!routeParams.hasTruthyParamValue('asset')) {
                    dataPump.requestUpdate();
                }
                timeProvider.notifyOnNextUpdate(true, true);
            }
            if (isNewRange) {
                $rootScope.$broadcast('resetTimebarEvent', {
                    start: timeParameters.starttime, end: timeParameters.endtime
                });
                if (displayProvider.containsSymbolsWithDataSources) {
                    $rootScope.$broadcast('dataSourcesExistToEnableTimebarEvent');
                }
            }
        }

        function setupToolbarsAndPanes() {
            var footer = paneSplitters.getFooterSplitter();
            if (footer) {
                paneSplitters.hideFooterSplitter();

                var container = $('#footer-pane');
                if (container) {
                    container.addClass('c-footer-overflow');
                }

                var pane = footer.options.panes[1];
                pane.collapsible = true;
                footer.expand('#footer-pane');
                pane.collapsible = false;
            }

            forceSplitterResize();
        }

        function loadComplete(serverDisplay, forcePostDisplayOnFirstRequest) {

            // NOTE: serverDisplay is the raw display object return from the server
            // IT IS NOT the same object as displayProvider (or even displayProvider.instance)
            // but it is the raw object retured by the server when we opened/created this display
            // It is used by the display provider to initalize its own state.
            // We should not have been given this object at all since the display provider should
            // be our abstraction on a "display". However, we need the saved display time range/event frame
            // from this object, and the display provider doesn't have that as part of its state.
            // Maybe the display provider should just have sent us some kind of new 
            // object that includes this time data, or it should add these values as properties on itself?
            // For now just read what we need from this object immediately and then ignore it.
            var startTime = serverDisplay.StartTime;
            var endTime = serverDisplay.EndTime;
            var eventFramePath = serverDisplay.EventFramePath;

            displayLoaded = true;

            setKioskMode(routeParams.isKiosk);

            // set layout for new displays (id === -1), BUT NOT if there's no toolbar, kiosk mode is on (a new display in kiosk mode, that's odd),
            // or we're opening an ad hoc display (which also will have a -1 display ID)
            setLayoutMode(displayProvider.displayId === -1 && !routeParams.hasTruthyParamValue('hidetoolbar') && !vm.kioskMode && !vm.isAdhocDisplay);

            // save display name
            pendingDisplayName = displayProvider.displayName;
            lastDiffedName = displayProvider.displayName;
            setTitle(displayProvider.displayName);

            // Change asset paths if requested
            if (vm.substituteDatasources(routeParams.getParamValue('asset'), routeParams.getParamValue('rootpath'))) {
                forcePostDisplayOnFirstRequest = true;
            }

            // check state
            lastSavedState = getCurrentState();
            displayProvider.canSaveAs = canSaveCheck();

            // initialize context and time
            eventCache.init();
            timeProvider.init();
            assetContext.init();

            // forcing a post of the display initally means that some or all of the display
            // was created locally when this view was loaded. 
            if (!forcePostDisplayOnFirstRequest) {

                // if we're forcing a post, we'll make this request after the first 
                // post returns to ensure the COG is populated in an adhoc display
                assetContext.getRelatedAssets();

                // restore cached display state
                var cachedState = displayState.getState();
                if (cachedState) {
                    if (cachedState.displayTime) {
                        startTime = cachedState.displayTime.start;
                        endTime = cachedState.displayTime.end;
                    }

                    if (cachedState.eventFramePath) {
                        eventFramePath = cachedState.eventFramePath;
                    }
                }
            }

            // sign-up for chronicle notifications
            chronicleProvider.reset();

            // note chronicle/chronicleProvider does not individually notify us which variable changes.
            // the callbacks on the addToWatch calls below will all fire when *anything* changes.
            // we could just combine all the code in these callback into one function (register it
            // using addOnUndoFunction and addOnRedoFunction)
            // However it would be nice to refactor chronicle to support telling us exactly what did change.
            // It would also be nice if we could get it to automatically set the watched varaible on and
            // undo/redo. It can do this, but our chronicleProvider wrapper breaks this.
            // Even better would be to remove chronicleProvider and just make chronicle itself
            // support watching of multiple variables the *right* way.
            chronicleProvider.addToWatch('display', displayProvider.instance, function () {
                displayProvider.applyDisplayState(chronicle.scope.watchObjects.display);
            });

            chronicleProvider.addToWatch('time', timeProvider.displayTime, function () {
                timeProvider.applyNewDisplayTimeObject(chronicle.scope.watchObjects.time, true);
            });

            chronicleProvider.addToNoWatch('lastRequestedTime', timeProvider.model.state.lastRequestedTimes, function () {
                var time = chronicle.scope.noWatchObjects.lastRequestedTime;
                timeProvider.model.revertLastRequested(time.start, time.end);
            });

            chronicleProvider.addToWatch('timeRangeEvent', timeProvider.timeRangeEvent, function () {
                timeProvider.applyNewTimeRangeEventObject(chronicle.scope.watchObjects.timeRangeEvent, true);
            });

            chronicleProvider.addToWatch('swappedAssets', assetContext.swappedAssets, function () {
                assetContext.swappedAssets = chronicle.scope.watchObjects.swappedAssets;
            });

            chronicleProvider.init();
            chronicle = chronicleProvider.chronicle;
            chronicle.addOnAdjustFunction(onUserChange);
            chronicle.addOnUndoFunction(onUndo);
            chronicle.addOnRedoFunction(onRedo);

            // initialize diff generator and start data pump
            diffGenerator.init();
            editorData = new PV.editorData($q, dataPump, diffGenerator, displayData, displayProvider, timeProvider, webServices, PiToast, log, assetContext);
            editorData.init(forcePostDisplayOnFirstRequest);

            startDataPumpForUrlParametersOrRange(startTime, endTime, eventFramePath);
            lastSavedProperties = getSavedProperties();

            // Refresh icons visibility/accessibility
            refreshCanPaste();
            $rootScope.$broadcast('loadComplete');

            if (routeParams.hasTruthyParamValue('asset')) {
                // make a web service call to find related assets on the display for each asset in the url parameter and then swap assets and request an update
                matchAssets(routeParams.getParamValue('asset'));
            }

            var fullUrl = $location.absUrl(),
                hashLoc = fullUrl.indexOf('#'),
                embedLoc = fullUrl.indexOf('embed=popup');

            if (embedLoc > -1 && hashLoc > -1 && hashLoc > embedLoc) {
                displayProvider.isEmbeddedAsPopup = true;
            }

            setFooterHeight();
        }

        // Process the 'asset' and 'rootpath' URL parameters
        function substituteDatasources(asset, rootpath, swappedAssets) {
            if (asset && asset.indexOf(';') === -1 && asset.indexOf('|') === -1 && asset.match(/\\\\[^\\]+\\[^\\]+\\[^\\]+/)
                      && displayProvider.getDistinctAssetsFromSymbolDatasources(!!swappedAssets).length === 1) {
                displayProvider.replaceAssetsInSymbolDatasources('asset', 'af:' + asset, swappedAssets);
                return true;
            }

            if (rootpath && rootpath.indexOf(';') === -1 && rootpath.indexOf('|') === -1 && rootpath.match(/\\\\[^\\]+/)) {
                displayProvider.replaceAssetsInSymbolDatasources('rootpath', 'af:' + rootpath, swappedAssets);
                return true;
            }
        }

        function getSavedProperties() {
            return {
                displayTime: JSON.stringify(timeProvider.displayTime),
                timeRangeEvent: JSON.stringify(timeProvider.timeRangeEvent),
                eventFrameSearch: displayProvider.eventFrameSearch ? JSON.stringify(displayProvider.eventFrameSearch.instance) : null,
                FitAll: displayProvider.FitAll
            };
        }

        function getAdHocDisplayArgs() {
            var displayId = routeParams.getDisplayId();
            return {
                dispId: displayId > -1 ? displayId : -1,
                reqId: routeParams.getParamValue('requestid'),
                adHocType: routeParams.hasTruthyParamValue('symbol') ? routeParams.getParamValue('symbol') : 'table',
                symbols: routeParams.hasTruthyParamValue('symbols') ? routeParams.getParamValue('symbols') : routeParams.getParamValue('openadhocdisplay'),
                currentElement: routeParams.hasTruthyParamValue('currentElement') ? routeParams.getParamValue('currentelement') : null
            };
        }

        function initializeDisplay() {

            var toastErrorOptions = {
                waitTime: 99999999,
                location: {
                    top: '50%', left: '50%'
                },
                fontSize: '24px'
            };

            setupToolbarsAndPanes();

            if (routeParams.isKiosk) {
                vm.kioskMode = true;
            }

            // open an ad hoc display based on a existing PIVisualization or PB display
            // this is the format used by the ad hoc button on editor and PBViewer displays
            if (routeParams.isDisplayBasedAdHoc) {
                var MaxParamLength = 1500; // this should go into some global constants repository eventually
                if (routeParams.getParamValue('openadhocdisplay').length > MaxParamLength) {
                    PiToast.error(PV.ResourceStrings.TooManySelectedSymbolsError, toastErrorOptions);
                    log.add(PV.ResourceStrings.DisplayError, log.Severity.Error, PV.ResourceStrings.TooManySelectedSymbolsError, log.ClearType.NewDisplay);
                    $state.go('Start');
                }

                var adHocArgs = getAdHocDisplayArgs();

                displayProvider.loadAdHocDisplay(
                    adHocArgs.dispId,
                    adHocArgs.reqId,
                    adHocArgs.symbols,
                    adHocArgs.adHocType,
                    adHocArgs.currentElement,
                    loadCompleteAdHoc,
                    loadError);

            } else if (routeParams.getDisplayId() > -1) {
                // open an existing normal display              
                displayProvider.loadDisplay(routeParams.getDisplayId(), loadComplete, loadError);

            } else if (routeParams.isAdHoc) {
                // Create a new ad hoc display locally with a list of data items on the URL.
                // Unlike the other ad hoc display URL format, this is not based on any existing display
                // but creates a brand new display locally using just those data sources in the URL.
                // This is used by external applications to integrate with PIVisualization by linking or framing.

                if (routeParams.hasTruthyParamValue('dataitems')) {
                    var rawDataSources = routeParams.getParamValue('dataitems').split(';');
                    var dataSourcesToResolve = [];
                    var dataSources = [];

                    // if we have an element in the datasource list, separate it out, and resolve it into attributes, and
                    // then recombine the resolved datasources with the datasources that did not need resolution
                    rawDataSources.forEach(function (ds) {
                        if (isAfDatasource(ds)) {
                            if (ds.indexOf('|') > 0) {
                                dataSources.push(CONSTANTS.AF_SERVER_PREFIX + ds);
                            } else {
                                var dsItem = {
                                    IsAsset: true, IsDisabled: true, IsPlottable: true, Name: 'Name', Path: CONSTANTS.AF_SERVER_PREFIX + ds, Title: ds, Type: 'asset'
                                };
                                dataSourcesToResolve.push(dsItem);
                            }
                        } else {
                            dataSources.push('pi:' + ds);
                        }
                    });

                    if (dataSourcesToResolve.length > 0) {
                        var completeResolve = function (resolvedDataSources) {
                            var mergedDataSources = dataSources.concat(resolvedDataSources);
                            displayProvider.createAdHocDisplay(routeParams.getParamValue('symbol') || 'trend', mergedDataSources, loadCompleteAdHoc);
                        };

                        displayProvider.resolveItemsWithDatasources(dataSourcesToResolve, completeResolve);
                    } else {
                        displayProvider.createAdHocDisplay(routeParams.getParamValue('symbol') || 'trend', dataSources, loadCompleteAdHoc);
                    }
                }
            } else {
                // open a new, empty display ready to be editied (this is where the New Display button links to)
                displayProvider.createNewDisplay(loadComplete);
            }

            function loadCompleteAdHoc(serverDisplay) {
                vm.isAdhocDisplay = true;
                loadComplete(serverDisplay, true);
                checkDirtyDisplay();
                adHocSymbol = displayProvider.instance.Symbols[0];
            }

            function loadError() {
                PiToast.error(PV.ResourceStrings.PBDisplayLoadError, toastErrorOptions);
                log.add(PV.ResourceStrings.DisplayError, log.Severity.Error, PV.ResourceStrings.PBDisplayLoadError, log.ClearType.NewDisplay);

                if (routeParams.isKiosk) {
                    // there is a ton of setup on the display that is skipped when an error occurs (see loadComplete())
                    // this was fine when we left this page immediately, but now we stay on this screen in kiosk mode
                    // I suspect there could be lurking bugs if the user trys to interact with this broken display
                    setKioskMode(true);
                    setLayoutMode(false);
                } else {
                    $state.go('Start');
                }
            }

            function isAfDatasource(datasource) {
                var slashCount = (datasource.match(/\\/g) || []).length;
                return (datasource.indexOf('|') !== -1 || slashCount > 3);
            }
        }

        function onDropOverSymbol(currSymbol, $draggedElement) {
            displayProvider.selectSymbol(currSymbol.Name);

            // Just return if drop is disabled.
            if ($draggedElement && $draggedElement.length > 0 && $draggedElement.hasClass('drop-disabled')) {
                return;
            }

            if ($draggedElement[0].draggedItems &&
               isEventDragDropAllowed(currSymbol.Configuration, null, $draggedElement[0].draggedItems)) {
                return;
            }

            vm.busyIndicator = true;
            var def = vm.getRuntimeSymbolData(currSymbol.Name).def;

            // TODO: PI Tags have PersistPath property while AF attributes have only Path.
            // Use PersistPath when available otherwise Path.
            displayProvider.resolveItemsWithDatasources($draggedElement[0].draggedItems, function (datasources) {

                if (datasources && datasources.length > 0) {
                    // Multistate configuration is automatically triggered if the symbol is not originally dynamic.
                    // If dynamic, multistate configuration panel open is triggered by right-click, not by this mechanism.

                    if (def.StateVariables && def.datasourceBehavior === PV.Extensibility.Enums.DatasourceBehaviors.None) {
                        showConfigPane({
                            title: PV.ResourceStrings.ConfigureMultistateOption,
                            mode: 'initialize-multistate',
                            datasource: datasources[0]
                        });
                    } else {
                        mergeNewDataSourcesIntoSymbol(currSymbol, datasources, $draggedElement[0].draggedItems);

                        if (!def.openConfigPane) {
                            $rootScope.$broadcast('closeConfigPane');
                        }
                        timeOutRequestUpdate();
                    }
                }
            });
        }

        function mergeNewDataSourcesIntoSymbol(symbol, datasources, items) {
            var rt = displayProvider.getRuntimeSymbolData(symbol.Name);
            if (rt && rt.def) {
                var def = rt.def;

                if (!symbol.DataSources) {
                    symbol.DataSources = [];
                }

                var startLength = symbol.DataSources.length;

                if (def.datasourceBehavior === PV.Extensibility.Enums.DatasourceBehaviors.Multiple) {
                    if (Array.isArray(datasources)) {
                        datasources.forEach(function (ds) {
                            symbol.DataSources.push(ds);
                        });
                    } else if (typeof datasources === 'string') {
                        symbol.push(datasources);
                    }
                } else {
                    if (typeof datasources === 'string') {
                        symbol.DataSources = [datasources];
                    }
                    else if (Array.isArray(datasources) && datasources.length === 1) {
                        symbol.DataSources = datasources;
                    }
                }

                if (def.dataSourcesAdded) {
                    // Allow symbol to adjust configuration without using $watch, avoid extra undo states
                    def.dataSourcesAdded(symbol.Configuration, symbol.DataSources, symbol.DataSources.length - startLength, items, datasources);
                }

                if (def.openConfigPane) {
                    def.openConfigPane($rootScope, symbol.DataSources);
                }
            }
        }

        function onNameChange() {
            if (pendingDisplayName !== lastDiffedName) {

                // create a diff for name change only
                var renamedDisplay = displayProvider.generateServerDisplay(null, pendingDisplayName);
                diffGenerator.generateRenameDiff(renamedDisplay);

                lastDiffedName = pendingDisplayName;
                checkDirtyDisplay();
                return true;
            }

            return false;
        }

        function onRedo() {
            undoRedo(chronicle.archive[chronicle.currArchivePos - 1]);
        }

        function onUndo() {
            undoRedo(chronicle.archive[chronicle.currArchivePos + 1]);
        }

        function undoRedo(prev) {
            vm.busyIndicator = true;
            var current = chronicle.archive[chronicle.currArchivePos];

            // generate diffs on full server display objects so that display changes are properly formed
            diffGenerator.generateAndAddDiffIfNecessary(
                displayProvider.generateServerDisplay(prev.watchVar.display, null, null, null),
                displayProvider.generateServerDisplay(current.watchVar.display, null, null, null));

            notifyAssetContextAfterChanges(diffGenerator, current, prev);

            checkDirtyDisplay();
            // we need to set a property on the collection that undo/redo happened while collection was in edit mode
            // since we don't know what exactly has changed, we need to know that on dataUpdate the collection's runtime symbols have to be recreated 
            // also, it should apply to all display's collection symbols, not only if in edit mode 
            displayProvider.notifyCollectionOnUndoRedo();

            dataPump.requestUpdate();
            timeProvider.notifyOnNextUpdate(true, displayProvider.isCollectionInEditMode());
        }

        function isNoWatchTriggered() {
            var current = chronicle.archive[chronicle.archive.length - 1];
            var prev = chronicle.archive[chronicle.archive.length - 2];
            var currSymbols = current.watchVar.display.Symbols;
            var prevSymbols = prev.watchVar.display.Symbols;
            var noWatchTriggered = false;
            //only investigate watchs triggered by changes in symbols, not addition or removal of symbols
            if (currSymbols.length === prevSymbols.length) {
                $.each(currSymbols, function (i, sym) {
                    if (sym.Configuration && sym.Configuration.NoWatch) {
                        var currConfig = angular.copy(sym.Configuration);
                        var prevConfig = angular.copy(prevSymbols[i].Configuration);
                        var currNoWatch = sym.Configuration.NoWatch;
                        var prevNoWatch = prevSymbols[i].Configuration.NoWatch;
                        currConfig.NoWatch = null;
                        prevConfig.NoWatch = null;
                        if (!angular.equals(currNoWatch, prevNoWatch) && angular.equals(currConfig, prevConfig)) {
                            if (chronicle.archive.length - 1 <= vm.prevArchivePos) {
                                prevSymbols[i].Configuration.NoWatch = currNoWatch;
                                chronicle.archive.splice(-1, 1);
                            }
                            if (vm.prevArchivePos < chronicle.archive.length) {
                                chronicle.currArchivePos = vm.prevArchivePos;
                            }
                            noWatchTriggered = (currNoWatch.allowDirtyCheck ? false : true);
                            if (currNoWatch.allowDirtyCheck) {
                                currNoWatch.allowDirtyCheck = false;
                            }
                        }
                    } else if (!angular.equals(sym, prevSymbols[i])) {
                            noWatchTriggered = false;
                            return false;
                    }
                });
            }
            return noWatchTriggered;
        }

        function onUserChange() {
            if (chronicle.currArchivePos > 0) {

                // generate diffs on full server display objects so that display changes are properly formed
                var prev = chronicle.archive[chronicle.currArchivePos - 1];
                var current = chronicle.archive[chronicle.currArchivePos];

                diffGenerator.generateAndAddDiffIfNecessary(
                    displayProvider.generateServerDisplay(prev.watchVar.display, null, null, null),
                    displayProvider.generateServerDisplay(current.watchVar.display, null, null, null));

                notifyAssetContextAfterChanges(diffGenerator, current, prev);

                if (!isNoWatchTriggered()) {
                    vm.prevArchivePos = chronicle.currArchivePos;
                    checkDirtyDisplay();
                }
            }
        }

        function notifyAssetContextAfterChanges(diffGenerator, current, prev) {
            var contextChanged = !angular.equals((current.watchVar.display.DisplayProperties || {}).context, (prev.watchVar.display.DisplayProperties || {}).context);
            if (contextChanged || diffGenerator.dataSourcesChanged) {
                assetContext.onDisplayChange(contextChanged);
            }
        }

        function fireRequestUpdate() {
            if (vm.requestDataUpdate) {
                dataPump.requestUpdate(vm.defaultChangedSymbolsOnly);
            }
            vm.defaultChangedSymbolsOnly = false;
            vm.requestDataUpdate = false;
            vm.timeoutPromiseForRequestUpdate = null;
        }

        function timeOutRequestUpdate(changedSymbolsOnly, forceUpdate) {
            if (vm.timeoutPromiseForRequestUpdate) {
                $timeout.cancel(vm.timeoutPromiseForRequestUpdate);
                vm.timeoutPromiseForRequestUpdate = null;
            }

            if (!vm.requestDataUpdate) {
                vm.defaultChangedSymbolsOnly = !!changedSymbolsOnly;
            } else {
                if (vm.defaultChangedSymbolsOnly && !changedSymbolsOnly) {
                    vm.defaultChangedSymbolsOnly = false;
                }
            }
            vm.requestDataUpdate = true;
            if (forceUpdate) {
                displayProvider.forceUpdate = true;
            }
            vm.timeoutPromiseForRequestUpdate = $timeout(fireRequestUpdate, 0);
        }

        function pasteSymbol() {
            var oldIdsToNew = { };
            var oldNamesToNew = { };

            if (!vm.kioskMode) {
                // get symbols and associated data from clipboard
                var clipboard = appClipboard.getSymbols();
                if (!clipboard) {
                    return;
                }
                var clipboardSymbols = clipboard.symbols;
                if (!clipboardSymbols && clipboardSymbols.length < 1)
                    return;

                // if we have attachments and we're not copying to the same edited display we'll need to copy those attachments into this display
                // and setup a map so we can reset the AttachmentIds in the symbols to the new id of the attachments in this display
                var attachments = clipboard.attachments;
                var shouldCopyAttachments = attachments && attachments.length > 0 && clipboard.requestId !== displayProvider.requestId && attachments;
                if (shouldCopyAttachments) {
                    if (attachments && attachments.length > 0) {
                        attachments.forEach(function (attachment) {
                            var newId = displayProvider.addAttachment(attachment.data).id;
                            oldIdsToNew[attachment.id] = newId;
                        });
                    }
                }

                // Clear any selected symbol on display
                displayProvider.selectSymbol();
                var pasteDelayed = false;
                var posLeft = Infinity, posTop = Infinity;
                if (vm.copySource !== vm.pasteTarget && vm.pasteTarget === 'collection') {
                    pasteDelayed = true;
                }
                var topOffset  = (vm.pasteTarget === 'collection' && displayProvider.defaultCollectionSymbol) ? displayProvider.defaultCollectionSymbol.Configuration.Top : 0;
                var leftOffset = (vm.pasteTarget === 'collection' && displayProvider.defaultCollectionSymbol) ? displayProvider.defaultCollectionSymbol.Configuration.Left : 0; 

                // copy the symbols and attachments into the display or into the collection in edit mode
                clipboardSymbols.forEach(function (currClipboardSymbol) {
                    var clonedSym = displayProvider.cloneSymbol(currClipboardSymbol);
                    if (!displayProvider.isCollectionInEditMode() || canPasteIntoCollection(clonedSym)) {
                        if (displayProvider.isGroup(clonedSym.SymbolType)) {
                            clonedSym.Configuration.Children.forEach(function (child) {
                                oldNamesToNew[child] = clonedSym.Name;
                            });
                        }
                        else if (oldNamesToNew[currClipboardSymbol.Name]) {
                            var newParent = displayProvider.getSymbolByName(oldNamesToNew[currClipboardSymbol.Name]);
                            var children = newParent.Configuration.Children;
                            children.splice(children.indexOf(currClipboardSymbol.Name), 1);
                            children.push(clonedSym.Name);
                        }

                        if (shouldCopyAttachments) {
                            if (displayProvider.isCollection(clonedSym.SymbolType) && clonedSym.StencilSymbols) {
                                clonedSym.StencilSymbols.forEach(function (stencilSymbol) {
                                    if (stencilSymbol && stencilSymbol.Configuration && stencilSymbol.Configuration.AttachmentId !== undefined) {
                                        stencilSymbol.Configuration.AttachmentId = oldIdsToNew[stencilSymbol.Configuration.AttachmentId];
                                    }
                                });
                            }

                            if (shouldCopyAttachments && clonedSym.Configuration.AttachmentId !== undefined) {
                                clonedSym.Configuration.AttachmentId = oldIdsToNew[clonedSym.Configuration.AttachmentId];
                            }
                        }

                        if (pasteDelayed) {
                            //copying from display to collection - can't add new symbols yet, need to calculate relative offsets when copying from outside into collection
                            posLeft = Math.min(currClipboardSymbol.Configuration.Left, posLeft);
                            posTop = Math.min(currClipboardSymbol.Configuration.Top, posTop);
                        } else {
                            // move the cloned symbol down and to the right for each consecutive paste
                            clonedSym.Configuration.Top = topOffset + currClipboardSymbol.Configuration.Top + (25 * pasteCount);
                            clonedSym.Configuration.Left = leftOffset + currClipboardSymbol.Configuration.Left + (25 * pasteCount);
                            displayProvider.addSymbol(clonedSym);

                            // multi select all pasted symbols
                            displayProvider.selectSymbol(clonedSym.Name, false, true);
                            // show busyIndicator only if at least 1 symbol has datasources which also means turn off for symbols with no data sources like Shapes
                            if (!vm.busyIndicator && clonedSym.DataSources && clonedSym.DataSources.length > 0) {
                                vm.busyIndicator = true;
                            }
                        }
                    }
                });

                setLayoutMode(true);
                pasteCount++;
                if (pasteDelayed) {
                    // need to add the Collection offset
                    clipboardSymbols.forEach(function (currSym) {
                        if (canPasteIntoCollection(currSym)) {
                            var sym = displayProvider.cloneSymbol(currSym);
                            sym.Configuration.Top = topOffset + currSym.Configuration.Top - posTop;
                            sym.Configuration.Left = leftOffset + currSym.Configuration.Left - posLeft;
                            displayProvider.addSymbol(sym);
                            displayProvider.selectSymbol(sym.Name, false, true);
                        }
                    });
                    // preserve clipboard before copying again so it can be restored when exiting collection edit mode
                    // this is only needed if a copy was taken outside of collection
                   vm.savedClipboard = angular.copy(appClipboard.getSymbols());
                   appClipboard.setSymbols(displayProvider.getSelectedSymbols(true, true, false, true), attachments, displayProvider.requestId);
                   vm.copySource = vm.pasteTarget;
                   pasteCount = 1;
                }

                timeOutRequestUpdate();
            }
        }

        function canPasteIntoCollection(symbol) {
            if (symbol.SymbolType === 'collection') {
                return false;
            }

            var def = symbolCatalog.getDefinition(symbol.SymbolType);
            if (def && def.supportsCollections) {
                // Allows symbols with 'supportsCollections' flag set and by themselves are not collections (eg., table)
                // to be pasted into collections.
                return (symbol.Configuration && symbol.Configuration.RetrieveCollectionData) ? false : true;
            } else {
                return false;
            }
        }

        function redo() {
            chronicle.redo();
            vm.prevArchivePos = chronicle.currArchivePos;
        }

        function setArrangeButton(state) {
            vm.showArrangeMenu = state;
        }

        function toggleArrangeMenu() {
            vm.showArrangeMenu = !vm.showArrangeMenu;
        }

        function toggleShapeMenu() {
            vm.showShapeMenu = !vm.showShapeMenu;
        }

        function refreshButtons() {
            refreshCanCut();
            refreshArrangeButton();
        }

        function refreshArrangeButton() {
            refreshCanMove();
            refreshCanAlign();
            refreshCanDistribute();

            if (!vm.canMove && !vm.canAlign && !vm.canDistribute) {
                vm.showArrangeButton = false;
                vm.showArrangeMenu = false;
            } else {
                vm.showArrangeButton = true;
            }
        }

        function refreshCanDistribute() {
            vm.canDistribute = displayProvider.canDistribute();
        }

        function refreshCanAlign() {
            vm.canAlign = displayProvider.canAlign();
        }

        function refreshCanCut() {
            // todo: add a selected symbols count provider function?
            // Allow Cut/Paste is any symbol is selected
            var selectedSymbols = displayProvider.getSelectedSymbols();
            var canCut = !vm.kioskMode && selectedSymbols.length > 0;

            if (canCut && displayProvider.isCollectionInEditMode() && selectedSymbols.length === 1 && displayProvider.defaultCollectionSymbol.Name === selectedSymbols[0]) {
                canCut = false;
            }
            vm.canCut = canCut;
        }

        function refreshCanPaste() {
            // Allow Paste is there is any symbol in app clipboard
            var clipboard = appClipboard.getSymbols();

            var canPaste = clipboard && clipboard.symbols && clipboard.symbols.length > 0;
            if (canPaste && displayProvider.isCollectionInEditMode()) {
                canPaste = clipboard.symbols.some(function (symbol) {
                    var def = symbolCatalog.getDefinition(symbol.SymbolType);
                    return !!def.supportsCollections;
                });
                
            }

            vm.canPaste = canPaste;
        }

        function saveFullDisplay(savingAsNewCopy, folderId) {
            var displayRequest = {
                StartTime: timeProvider.displayTime.start,
                EndTime: timeProvider.displayTime.end,
                EventFramePath: timeProvider.timeRangeEvent.path,
                Display: displayProvider.generateServerDisplay(null, pendingDisplayName),
                Attachments: displayProvider.getAttachmentsForSave(savingAsNewCopy),
                CurrentElement: displayProvider.currentElement,
                FolderId: savingAsNewCopy && !isNaN(folderId) ? folderId : null
            };

            webServices.postSaveDisplay(displayRequest)
                .then(function (response) {
                    saveSuccess(response.data, savingAsNewCopy);
                })
                .catch(function (response) {
                    saveError(response);
                });
        }

        function saveAsDisplay(displayName, folderId) {
            // execute save inside timeout so that pending changes are
            // applied prior to saving (e.g. saving collection search criteria)
            $timeout(function () {
                // save the current display as a new display with the provided name
                displayProvider.displayId = -1;
                pendingDisplayName = displayName;
                saveFullDisplay(true, folderId);
            }, 0);
        }

        function saveDisplay(reload) {
            // execute save inside timeout so that pending changes are
            // applied and diffed prior to saving (e.g. setting display name)
            $timeout(function () {
                // stop updates
                dataPump.stop();

                var changeRequest = {
                    StartTime: timeProvider.displayTime.start,
                    EndTime: timeProvider.displayTime.end,
                    EventFramePath: timeProvider.timeRangeEvent.path,
                    Changes: diffGenerator.differences,
                    Attachments: displayProvider.getAttachmentsForSave()
                };

                if (displayData.forcePostDisplay) {
                    saveFullDisplay();
                } else {
                    // save using request id, if that fails (and only if that fails) save by display id
                    webServices.postSaveDisplayByRequestId(displayProvider.requestId, changeRequest)
                        .then(function (response) {

                            // Reloads the display if creating a new display or parameter already set.
                            // This is to prevent a bug where a new DisplayController was being created
                            // on a URL change when navigating from the Editor to the Start page
                            // which led to the dataPump sending updates on the start page and symbols 
                            // becoming frozen on the newly created display.
                            reload = reload || displayProvider.displayId === -1;

                            saveSuccess(response.data, reload);
                        })
                        .catch(function (response) {
                            if (response.status === 416 || response.status === 400) {
                                saveFullDisplay();
                            }
                            else {
                                saveError(response);
                            }
                        })
                        .finally(function () {
                            dataPump.requestUpdate();
                        });
                }
            }, 0);
        }

        function saveError(response) {
            var errorText = response.statusText;    // Default to the default 8-bit status string (Always English unless overridden)

            // If the response data is a non-empty string, use that for the error text
            if (typeof response.data === 'string' || response.data instanceof String) {
                if (response.data) {
                    errorText = response.data;
                }
            } else if (response.data && typeof response.data === 'object') {
                if (typeof response.data.Message === 'string' || response.data.Message instanceof String) {
                    errorText = response.data.Message;
                }
            }

            if (response.status === 403) {
                $rootScope.$broadcast('saveErrorEvent', errorText);
                showErrorToast(errorText);
            } else if (response.status === 412 || response.status === 404) {
                $rootScope.$broadcast('saveErrorEvent', errorText);
            } else {
                showErrorToast(errorText);
            }

            if (response.status !== 400) {
                diffGenerator.init();
            }
        }

        function saveSuccess(data, reload) {
            displayProvider.displaySaved(data);

            applyPendingDisplayName();
            lastDiffedName = displayProvider.displayName;
            setTitle(displayProvider.displayName);

            lastSavedState = getCurrentState();
            lastSavedProperties = getSavedProperties();

            Object.keys(assetContext.swappedAssets).forEach(function (key) {
                delete assetContext.swappedAssets[key];
            });
            displayProvider.canSave = false;
            displayProvider.isDirtyDisplay = false;

            diffGenerator.init();

            $rootScope.$broadcast('closeSaveDialogEvent');
            $state.transitionTo('Shell.EditorDisplay', { id: displayProvider.displayId, name: data.LinkName }, {
                notify: !!reload
            });
        }

        function applyPendingDisplayName() {
            displayProvider.displayName = pendingDisplayName;
        }

        function setTitle(displayName) {
            var title = 'IHC';
            if (displayName) {
                title += ' - ' + displayName.toString();
            }
            $window.document.title = title;
        }

        function showErrorToast(message) {
            if (message) {
                PiToast.error(PV.ResourceStrings.EditorDialogError + ' ' + message, {
                    waitTime: 99999999
                });
                log.add(PV.ResourceStrings.DisplayError, log.Severity.Error, message, log.ClearType.NewDisplay);
            } else {
                PiToast.error(PV.ResourceStrings.EditorDialogError + ' ' + PV.ResourceStrings.UnknownError, {
                    waitTime: 99999999
                });
                log.add(PV.ResourceStrings.DisplayError, log.Severity.Error, PV.ResourceStrings.UnknownError, log.ClearType.NewDisplay);
            }
        }

        function setKioskMode(mode) {
            vm.kioskMode = mode;
            refreshButtons();
        }

        function setLayoutMode(mode) {
            if (!mode && isCollectionInEditMode()) {
                exitEditSelectedSymbol();
            }

            vm.layoutMode = !!mode;
            vm.showToolbar = vm.layoutMode;

            if (vm.layoutMode) {
                if (!dataPump.isRunning) { 
                    // refresh datapump and remove cursors
                    if (displayProvider.hasTrends()) {
                        dataPump.requestUpdate();
                    }
                }
            } else {
                displayProvider.selectSymbol();
            }

            $scope.$emit('selectedSymbolChangeEvent');

            var splitter = paneSplitters.getDisplaySplitter();
            if (splitter) {
                $timeout(function () {
                    splitter.resize();
                }, 1);
            }
        }

        function toggleToolbar() {
            vm.showToolbar = !vm.showToolbar;
        }

        function undo() {
            chronicle.undo();
            vm.prevArchivePos = chronicle.currArchivePos;
        }

        function isMultipleDataSource() {
            if (vm.currentSymbol && vm.currentSymbol.dataSources && vm.currentSymbol.dataSources.length > 0 && vm.currentSymbol.dataSources[0]) {
                return (vm.currentSymbol.dataSources[0].IsCategory || vm.currentSymbol.dataSources[0].IsAsset) ? true : false;
            }
            return false;
        }

        // Call server to match assets on display to new assets by template
        function matchAssets(swapAssetList) {
            if (swapAssetList) {
                var assets = swapAssetList.split(';');
                vm.busyIndicator = true;
                var displayAssets = displayProvider.getDistinctAssetsFromSymbolDatasources();
                var matchedAssets = webServices.postMatchRelatedAssets(displayAssets, assets);
                matchedAssets.promise.then(
                    function (e) {
                        assetContext.selectedAsset = assets[0]; // Show first asset for related asset dropdown
                        swapAssets(e.data);
                        dataPump.requestUpdate();
                    },
                    function (error) {
                        log.add(PV.ResourceStrings.DisplayError, log.Severity.Error, error.statusText, log.ClearType.Manual);
                        dataPump.requestUpdate();
                    }
                ).finally(function() {
                    vm.busyIndicator = false;
                });
            }
        }

        function swapAssets(assets) {
            if (assets && Array.isArray(assets) && assets.length > 0) {
                vm.busyIndicator = true;
                assets.forEach(function (assetsToReplace) {
                    if (assetsToReplace.To && assetsToReplace.From && assetsToReplace.From.length > 0) {
                        assetsToReplace.From.forEach(function (displayAsset) {
                            displayProvider.swapDisplayAssets(displayAsset, assetsToReplace.To, assetContext.swappedAssets);
                        });
                    }
                });
            }
        }

        function switchSymbolToCollection(message) {
            var switchComplete = function () {
                timeOutRequestUpdate();
                vm.showConfigPane(message.option);
            };

            displayProvider.deleteSelectedSymbolSwaps(assetContext.swappedAssets);
            displayProvider.switchSymbolToCollection(!!(message.single), switchComplete);
        }

        function switchSymbolToType(context, newType) {
            displayProvider.deleteSelectedSymbolSwaps(assetContext.swappedAssets);
            displayProvider.switchSymbolToType(context, newType);
            timeOutRequestUpdate();
        }

        function dragOut(event, ui) {
            var $dragElem = $(ui.helper[0]);
            $dragElem.addClass('drop-disabled');

            var items = $dragElem[0].draggedItems;
            var typeName = (items && items.type) || displayProvider.defaultSymbol;
            var def = symbolCatalog.getDefinition(typeName);

            // Hide image icon and show drag item labels.
            $dragElem.children('#symbolIcon:first').css('display', 'none');
            $dragElem.children('.add-child-template').css('display', 'block');
            $dragElem.removeData('SymbolMouseEnterCalledFirst');
            $dragElem.removeClass(def.defaultAllowDataSourceClass);

            // Apply custom transform if it exists
            items && items.dragTransform && items.dragTransform($dragElem);
        }

        function dragOver(event, ui) {
            var $dragElem = $(ui.helper[0]);
            $dragElem.addClass('drop-hover');

            var items = $dragElem[0].draggedItems;
            var typeName = (items && items.type) || displayProvider.defaultSymbol;
            var def = symbolCatalog.getDefinition(typeName);

            if (items && isEventDragDropAllowed(null, def, items)) {
                $dragElem.addClass('drop-disabled');
                $dragElem.removeClass('drop-hover');
            }

            def.defaultAllowDataSourceClass && $dragElem.addClass(def.defaultAllowDataSourceClass);

            var $dragImage = $dragElem.children('#symbolIcon:first');
            var scale = appData.zoomLevel;
            
            // Return if not initial drag image
            if ($dragImage.length !== 0) {
                return;
            }          

            // Make sure overflow is hidden and text white and not-wrapped.
            // Alpha for background color is enough to let text or image show up over light backgrounds
            $dragElem.css({
                'overflow': 'hidden',
                'background-color': 'rgba(0, 0, 0, 0.2)'
            });
            $dragElem.children('.add-child-template').css({
                'color': 'white'
            });

            // Apply custom transform if it exists
            items && items.dragTransform && items.dragTransform($dragElem, true);

            // Create initial drag image when dragging over the display
            if (def.datasourceBehavior !== PV.Extensibility.Enums.DatasourceBehaviors.None) {
                var symbolConfig = def.getDefaultConfig();
                var symbolHeight = symbolConfig.Height * scale;
                var symbolWidth = (symbolConfig.Width || 100) * scale;                

                // For singleton symbols, when multiple items are dragged, show a multiple symbol creation icon.
                // Icon file for this is regular file is appended with '_multiple.svg'.
                var dragImageIcon = def.iconUrl;

                if (def.datasourceBehavior === PV.Extensibility.Enums.DatasourceBehaviors.Single &&
                    ($dragElem.children('.add-child-template').length > 1 || (items &&
                    (items.length > 1 || items[0].IsCategory || items[0].IsAsset || items[0].IsEvent)))) {

                    dragImageIcon = dragImageIcon.substring(0, dragImageIcon.lastIndexOf('.svg'));
                    dragImageIcon = dragImageIcon + '_multiple.svg';
                }

                // If zoomed in, if symbols size is less than the the default image icon (size + padding), reset the icon size to avoid showing the icon cut off.
                var imageSize = 50;
                var symbolMinSize = Math.min(symbolWidth, symbolHeight);
                if (symbolMinSize + (symbolMinSize / 10) <= imageSize) {
                    imageSize = symbolMinSize - (symbolMinSize / 10);
                }

                // Create a div with symbol icon and append to the dragged element.
                $dragImage = $('<div></div>', {
                    id: 'symbolIcon',
                    width: symbolWidth,
                    height: symbolHeight,
                    display: 'block'
                });
                $dragImage.css({
                    'background-image': 'url(' + dragImageIcon + ')',
                    'background-repeat': 'no-repeat',
                    'background-size': imageSize * 1.2 + 'px ' + imageSize * 1.2 + 'px',
                    'background-position': 'center center',
                    'background-color': 'rgba(255,255,255,0.2)',
                    'opacity': '0.6'
                });
                $dragElem.append($dragImage);
            }

            // Drag image display handling is done in the draggable config 
            // while dragging since it depends on what element is the top
            // most element
        }

        //determines if a drop or drag event should be blocked
        //if the dragged context status as an event does not match
        //the symbols status as an event symbol
        function isEventDragDropAllowed(config, def, dragItems) {
            var block = false;

            if (!config && def.getDefaultConfig) {
                config = def.getDefaultConfig();
            }

            if (config) {
                $.each(dragItems, function (i, item) {
                    if (item.IsEvent !== config.isEvent) {
                        block = true;
                        return;
                    }
                });
            }

            return block;
        }

        function addSymbolsFromDrop(x, y, items) {
            var typeName = (items && items.type) || displayProvider.defaultSymbol;

            // Quit early if things are wrong.
            if (!displayProvider || !items || items.length === 0 || !typeName || typeName.length < 1) {
                vm.busyIndicator = false;
                return;
            }

            var def = symbolCatalog.getDefinition(typeName);

            if (isEventDragDropAllowed(null, def, items)) {
                vm.busyIndicator = false;
                return;
            }
            var newSymbol;
            var completeAdd = function (resolvedDSes) {
                if (resolvedDSes.length < 1) {
                    return;
                }

                var config = {};
                if (def.dataSourcesAdded) {
                    def.dataSourcesAdded(config, resolvedDSes, resolvedDSes.length, items);
                }

                var newSymbols = [];
                if (def.datasourceBehavior === PV.Extensibility.Enums.DatasourceBehaviors.Multiple) {
                    newSymbol = displayProvider.addSymbol(typeName, x, y, resolvedDSes, config);
                    newSymbols.push(newSymbol);
                } else {
                    resolvedDSes.forEach(function (ds) {
                        var sym = displayProvider.addSymbol(typeName, x, y, ds, config);
                        var pos = displayProvider.getRuntimeSymbolData(sym.Name).position;
                        newSymbols.push(sym);

                        // TODO: the 220 magic numer is a hack for auto width symbols, hopefully we can do something better here
                        // 220 is approx what a value symbol's width is if date is the longest string given the current default
                        // height of a value symbol
                        var width = pos.width ? pos.width : 220;
                        x += width + 20;
                    });
                }

                displayProvider.selectSymbol();
                newSymbols.forEach(function (sym) {
                    displayProvider.applyDisplayThemeToSymbol(sym);
                    displayProvider.selectSymbol(sym.Name, false, true);
                });

                $scope.$emit('selectedSymbolChangeEvent', true);
                $scope.$emit('closeShapeMenuEvent');
                $scope.$emit('setLayoutModeEvent', true);
                timeOutRequestUpdate();
            };

            if (def.datasourceBehavior === PV.Extensibility.Enums.DatasourceBehaviors.None) {                
                var sym = displayProvider.addSymbol(typeName, x, y, [], items[0].config);
                displayProvider.selectSymbol(sym.Name);

                vm.busyIndicator = false;
                $scope.$emit('selectedSymbolChangeEvent', true);
                $scope.$emit('closeShapeMenuEvent');
                $scope.$emit('setLayoutModeEvent', true);
            } else {
                displayProvider.resolveItemsWithDatasources(items, completeAdd);
            }
            
            if (def.openConfigPane && newSymbol) {
                def.openConfigPane($rootScope, newSymbol.DataSources);
            }
        }

        function dropOn(event, ui, element) {
            vm.busyIndicator = true;
            var $draggedElement = $(ui.helper[0]);

            $timeout(function () {
                // TODO: Symbols themselves handle drag (over) and drop without actually using 
                // JQUI Draggable. This leads to weird behavior where the draggable api thinks 
                // it should still drop on the display even if you dropped on a symbol.
                // We're working around below. We should make symbols drop targets and maybe this would be cleaner.
                if (cancelDrop) {
                    cancelDrop = false;
                    //prevent edge case on dropInStencil
                    dropInStencil = false;
                    vm.busyIndicator = false;
                    event.stopPropagation();
                    return;
                }

                // prevent drop outside edit area when in collection edit mode
                if (vm.isCollectionInEditMode()) {
                    if (!dropInStencil) {
                       vm.busyIndicator = false;
                       event.stopPropagation();
                       return;
                    }
                    dropInStencil = false;
                }

                var scale = appData.zoomLevel;

                if ($draggedElement && $draggedElement.hasClass('drop-disabled')) {
                    event.stopPropagation();
                    vm.busyIndicator = false;
                } else if ($draggedElement && $draggedElement.length > 0) {
                    var x = (event.pageX - element.offset().left + element.scrollLeft()) / scale;
                    var y = (event.pageY - element.offset().top + element.scrollTop()) / scale;

                    $draggedElement.removeClass('drop-hover');
                    addSymbolsFromDrop(x, y, $draggedElement[0].draggedItems);
                }
            }, 0);
        }

        // Turn spinner on/off for timerange changes
        timeProvider.onDisplayTimeChanged.subscribe(function () { vm.busyIndicator = true; });
        timeProvider.onDisplayTimeUpdated.subscribe(function () { vm.busyIndicator = false; });

        $scope.$on('assetSelectedEvent', function () { vm.busyIndicator = true; });

        $scope.$on('resizeContentEvent', function () {
            if (adHocSymbol && displayProvider.instance && displayProvider.instance.Symbols && displayProvider.instance.Symbols.length === 1 && displayProvider.instance.Symbols[0].Name === adHocSymbol.Name) {
                var runtime = displayProvider.getRuntimeSymbolData(adHocSymbol.Name);
                var positions = {
                    left: runtime.position.left,
                    top: runtime.position.top,
                    height: runtime.position.height,
                    width: runtime.position.width
                };

                if (!adHocPositions || JSON.stringify(adHocPositions) === JSON.stringify(positions)) {
                    displayProvider.calcSymbolSize(adHocSymbol.SymbolType, adHocSymbol.DataSources, adHocSymbol.Configuration);
                    runtime.update(adHocSymbol);
                    adHocPositions = {
                        left: runtime.position.left,
                        top: runtime.position.top,
                        height: runtime.position.height,
                        width: runtime.position.width
                    };
                } else {
                    adHocSymbol = undefined;
                }
            } else {
                adHocSymbol = undefined;
            }
        });

        // Rubberband selection
        function rubberbandStart(event) {
            if (vm.drawingModeName === 'select' && displayProvider.instance) {
                var displayPosition = displayProvider.getDisplayAreaPositionObject();
                vm.topOffset = vm.showToolbar ? $('#display-toolbar-pane').height() : 0;
                multiselectService.startRubberbandSelect({
                    x: event.pageX - displayPosition.leftWithoutScroll,
                    y: event.pageY - displayPosition.topWithoutScroll
                });
                $(document).bind('mousemove.rubberband', $scope, updateRubberband);
                $(document).on('mouseup.rubberband', clearRubberbandSelect);
            }
        }

        function updateRubberband(event) {
            var displayPosition = displayProvider.getDisplayAreaPositionObject();
            multiselectService.updateRubberbandSelect({
                x: event.pageX - displayPosition.leftWithoutScroll >= 0 ? event.pageX - displayPosition.leftWithoutScroll : 0,
                y: event.pageY - displayPosition.topWithoutScroll >= 0 ? event.pageY - displayPosition.topWithoutScroll : 0
            });
            $scope.$apply();
        }

        function clearRubberbandSelect() {
            $scope.$emit('selectedSymbolChangeEvent', true);
            $(document).unbind('mousemove.rubberband');
            $(document).off('mouseup.rubberband');
            multiselectService.clearRubberband();
            $scope.$apply();
        }
    }

})(window.PIVisualization);
