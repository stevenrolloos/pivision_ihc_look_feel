﻿// <copyright file="pbviewer.navigation.js" company="OSIsoft, LLC">
// Copyright © 2013-2017 OSIsoft, LLC. All rights reserved.
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF OSIsoft, LLC.
// USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT THE PRIOR EXPRESS WRITTEN
// PERMISSION OF OSIsoft, LLC.
//
// RESTRICTED RIGHTS LEGEND
// Use, duplication, or disclosure by the Government is subject to restrictions
// as set forth in subparagraph (c)(1)(ii) of the Rights in Technical Data and
// Computer Software clause at DFARS 252.227.7013
//
// OSIsoft, LLC.
// 1600 Alvarado Street, San Leandro, CA 94577
// </copyright>

/// <reference path="_references.js" chutzpah-exclude="true" />

window.PBDisplays = window.PBDisplays || {};
window.PBDisplays.Utils = window.PBDisplays.Utils || {};

(function (VMs, Views, Utils) {
    'use strict';

    Utils.createDisplayViewModel = function () {
        var needInit = !window.PBDisplays.displayViewModel ||
            !window.PBDisplays.displayView ||
            !window.PBDisplays.timebarViewModel;

        // initialize view model
        if (needInit) {
            var displayView = window.PBDisplays.displayView || new Views.DisplayView(),
                timebarViewModel = window.PBDisplays.timebarViewModel || new VMs.TimebarViewModel(),
                displayViewModel = window.PBDisplays.displayViewModel || new VMs.DisplayViewModel(
                    window.PBDisplays.webServices,
                    timebarViewModel,
                    window.PIVisualization.ResourceStrings,
                    window.PBDisplays.log,
                    displayView
                );

            displayView.initializeView(displayViewModel, timebarViewModel);
            window.PBDisplays.displayView = displayView;
            window.PBDisplays.displayViewModel = displayViewModel;
            window.PBDisplays.timebarViewModel = timebarViewModel;
        }
    };

    // setup client side navigation
    Utils.setClientRouting = function () {
        var app;

        function checkKioskMode(mode) {
            var isKioskMode = (/kiosk/i).test(mode);

            if (isKioskMode) {
                $('#toolbar-homepage-enabled').css('display', 'none');
                $('#toolbar-homepage-disabled').css('display', 'inline');
                $('#toolbar-helpButton-enabled').css('display', 'none');
                $('#toolBar-itemNewDisplay').css('display', 'none');
                $('#toolbar-itemAdHocButton').css('display', 'none');
                $('#toolbar-itemEmail').css('display', 'none');
                $('#toolbar-itemUserMenu').css('display', 'none');
            }
            else {
                $('#toolbar-homepage-enabled').css('display', 'inline');
                $('#toolbar-homepage-disabled').css('display', 'none');
                $('#toolbar-helpButton-enabled').css('display', 'inline');
                $('#toolBar-itemNewDisplay').css('display', 'inline');
                $('#toolbar-itemAdHocButton').css('display', 'inline');
                $('#toolbar-itemEmail').css('display', 'inline');
            }
        }

        function checkTimeToolParams(hideTime, hideTool) {
            if (hideTime) {
                $('.timebar-show').css('display', 'none');
            }
            if (hideTool) {
                $('.toolbar-show').css('display', 'none');
            }
        }

        // parse 'splat' parameters to get display id
        function getId(params) {
            var id = getPath(params);
            if (id && id.indexOf('/') > 0) {
                id = id.substr(0, id.indexOf('/'));
            }

            return parseInt(id, 10);
        }

        function getPath(params) {
            return (params && params.splat.length > 0) ? params.splat[0] : null;
        }

        // Sets the document title for the input display name.
        // The input can be a string, an observable, or a function that returns a string.
        function setTitle(displayName) {
            var title = 'PI Vision';
            if (displayName) {
                if (!$.isFunction(displayName)) {
                    title += ' - ' + displayName.toString();
                } else {
                    // See if it is a knockout observable
                    if (displayName.subscribe && $.isFunction(displayName.subscribe)) {
                        displayName.subscribe(function (newValue) {
                            setTitle(newValue);
                        });

                    } else {
                        setTitle(displayName());
                    }
                }
            }
            document.title = title;
        }

        function showPBViewer(params, displayId) {
            var currentElement = params.CurrentElement,
                startTime = params.StartTime,
                endTime = params.EndTime,
                timeZone = params.Tz,
                mode = params.Mode,
                hideTimebar = params.HideTimebar,
                hideToolbar = params.HideToolbar;

            PIVisualization.DateTimeFormatter.tzClientOverride = timeZone;
            Utils.createDisplayViewModel();

            if (displayId) {
                // If we are passed an integral display ID, use that to load the display
                // If we are passed the path to the file, use that to load the display
                // The loadDisplay function will figure out the difference
                window.PBDisplays.displayViewModel.loadDisplay(displayId, currentElement, startTime, endTime);
            } else {
                window.PBDisplays.displayViewModel.unloadDisplay(true);
            }

            // show or hide features
            if (window.sammyApp !== undefined) {
                checkKioskMode(mode);
                checkTimeToolParams(hideTimebar !== undefined, hideToolbar !== undefined);
            }
            if (window.PBDisplays.log) {
                window.PBDisplays.log.showDialog(false);
            }

            // show display
            $('#processbook').css('display', 'inline');
            setTitle(window.PBDisplays.displayViewModel.displayInfo.name);
        }

        (function ($) {
            /*jshint -W064*/
            app = Sammy(function () {
                this.get(/#\/pbdisplays\/(\d+)/i, function () {
                    Utils.normalizeObject(this.params, ['CurrentElement', 'StartTime', 'EndTime', 'Mode', 'Tz', 'HideTimebar', 'HideToolbar']);
                    showPBViewer(this.params, getId(this.params));
                });

                this.get(/#\/pbdisplays\/(\D\S*)/i, function () {
                    // This should only be encountered on the mobile site, but it is possible that it can be used anywhere
                    Utils.normalizeObject(this.params, ['CurrentElement', 'StartTime', 'EndTime', 'Mode', 'Tz', 'HideTimebar', 'HideToolbar']);
                    showPBViewer(this.params, getPath(this.params));
                });

                this.get(/#\/pbdisplayname\/(.*)/i, function () {
                    Utils.normalizeObject(this.params, ['CurrentElement', 'StartTime', 'EndTime', 'Mode', 'Tz', 'HideTimebar', 'HideToolbar']);
                    showPBViewer(this.params, getPath(this.params));
                });

                // Since the Search Input is part of a Form, Once Submit method is processed by KO binding,
                // SammyJS 'submit' binding calls '_checkFormSubmission' which in turn calls 'setLocation'
                // to add the '?search=xxx' parameter to the displayed URL. 
                // Overriding '_checkFormSubmission' here to prevent the URL being changed.
                this.defaultCheckFormSubmission = this._checkFormSubmission;
                this._checkFormSubmission = function (form) {
                    var $form = $(form);
                    if (this._getFormVerb($form) === 'get') {
                        return false;
                    }

                    return this.defaultCheckFormSubmission(form);
                };
            });
            /*jshint +W064*/

        })(jQuery);

        return app;
    };

})(window.PBDisplays.ViewModels, window.PBDisplays.Views, window.PBDisplays.Utils);
