# PIVision_IHC_look_feel

***** OVERALL ******

This bucket does 4 things to the PI Vision layout:

1) Changes colors from PIVision blue to IHC grey

2) Changes PI Vision logo to IHC logo

3) Changes title from "PI Vision - " to "IHC - "

4) Changes the icons from PI Vision icons to IHC icons

*** 1) COLOR CHANGES TO IHC GREY ***

Please have a look in "color_changes.xlsx" to see what files what code is changed to change colors from blue to grey


*** 2) PI Vision logo *** 

The PI Vision logo is changed in Scripts\app\editor\layout\pv-header-template.html, by editing "logo_pi" to "logo_ihc_big"

*** 3) Screen title "PI Vision - " to "IHC - " ***

Changed all text "PI Vision" to "IHC" in files:
	PIVision\Scripts\app\pbviewer\pbviewer.navigation.js
	
	PIVision\Scripts\app\editor\display\PIVisualization.display-controller.js
	
	PIVision\Scripts\app\m\pvdisplayviewmodel.js

Changed .title to "IHC" instead of variable text for
	PIVision\Views\Home\Index.cshtml

*** 4) Browser icon from  PI Vision icon to IHC icon ***

Put all new icons in the folder (overwriting PIvision icons)
	PIVision\Public\Images

Changed ?v2 in _AppIcons to make sure the browser recognizes the use of a new icon.
	\PIVision\Views\Shared\_AppIcons.cshtml voor de icon


